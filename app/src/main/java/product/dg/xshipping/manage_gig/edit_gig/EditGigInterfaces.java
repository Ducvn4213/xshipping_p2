package product.dg.xshipping.manage_gig.edit_gig;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;

import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.GigAdapter;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.adapter.model.GigItem;
import product.dg.xshipping.model.GigInfo;
import product.dg.xshipping.model.ReceiverInfo;
import product.dg.xshipping.model.SenderInfo;
import product.dg.xshipping.service.database.model.DistrictResponse;
import product.dg.xshipping.service.database.model.WardResponse;

public class EditGigInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void presentData(GigDetailItem data);
        void geoDataCallback(ArrayAdapter<String> provinceAdapter, ArrayAdapter<String> districtAdapter, ArrayAdapter<String> wardAdapter);
        void geoDataIndexCallback(int province, int district, int ward);
    }

    interface ProvidedPresenterOps {
        void loadData();
        void loadGeoData();

        void onSelectProvince(int id);
        void onSelectDistrict(int id);
        void endEndEdit();
        void updateGigTicket(String receiverName, String receiverPhone, String receiverAdrress,
                    String receiverProvince, String receiverDistrict, String receiverWard,
                    String senderAddress, String gigName, String gigWeight,
                    String gigPaymentMethod, String gigSize, String Date,
                    String gigCod, String gigNote, String gigBankName,
                    String gigBankNumber, String gigBankBank);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void handleOnDistrictCallback(DistrictResponse data);
        void handleOnWardCallback(WardResponse data);

        void notifyChanged(ArrayAdapter<String> districtAdapter, ArrayAdapter<String> wardAdapter);

        void finish();

        ArrayAdapter<String> createAdapterWithData(List<String> data);
        void geoDataCallback(ArrayAdapter<String> provinceAdapter, ArrayAdapter<String> districtAdapter, ArrayAdapter<String> wardAdapter,
                             int province, int district, int ward);
    }

    interface ProvidedModelOps {
        void loadDistricts(int province_id);
        void loadWards(int district_id);

        void handleOnDistrictCallback(DistrictResponse data);
        void handleOnWardCallback(WardResponse data);

        GigDetailItem getData();
        void setData(GigDetailItem item);
        void loadGeoData();

        void endUpdate();
        void updateGigTicket(ReceiverInfo receiverInfo, GigInfo gigInfo, SenderInfo senderInfo);
    }
}
