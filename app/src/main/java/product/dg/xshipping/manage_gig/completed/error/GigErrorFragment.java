package product.dg.xshipping.manage_gig.completed.error;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.GigAdapter;
import product.dg.xshipping.adapter.model.GigItem;

public class GigErrorFragment extends Fragment implements GigErrorInterfaces.RequiredViewOps {
    private GigErrorInterfaces.ProvidedPresenterOps mPresenter;

    ListView mGigListView;
    TextView mNoTicket;
    private ProgressDialog mProgressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_gig_fragment, container, false);

        bindingControls(view);
        setupControlEvents();

        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    private void bindingControls(View view) {
        mGigListView = (ListView) view.findViewById(R.id.lv_gig_manage);
        mNoTicket = (TextView) view.findViewById(R.id.no_ticket);
    }

    private void setupControlEvents() {
        mGigListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mPresenter.goToDetail(i);

//                GigItem data = mPresenter.getData(i);
//                GigErrorFragment.this.showViewErrorTicketOption(i, data);
                //mPresenter.goToDetail(i);
            }
        });
    }

    private void init() {
        GigErrorPresenter presenter = new GigErrorPresenter(this);
        GigErrorModel model = new GigErrorModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) GigErrorFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
            }
        });
    }

    @Override
    public void hideWaiting() {
        getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        });

    }

    @Override
    public void updateAdapter(GigAdapter adapter) {
        if (mGigListView != null) {
            mGigListView.setAdapter(adapter);
        }
    }

    void showViewErrorTicketOption(final int pos, GigItem data) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_option_show_error_ticket, null);
        dialogBuilder.setView(dialogView);

        TextView title = (TextView) dialogView.findViewById(R.id.tv_title);
        Button viewDetail = (Button) dialogView.findViewById(R.id.btn_view_detail);
        Button viewError = (Button) dialogView.findViewById(R.id.btn_view_error);

        title.setText(data.getTitle());

        final android.app.AlertDialog alertDialog = dialogBuilder.create();

        viewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.goToDetail(pos);
                alertDialog.dismiss();

            }
        });
        viewError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.showError(pos);
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void presentErrorTicket(String name, String data) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_error_ticket, null);
        dialogBuilder.setView(dialogView);

        TextView title = (TextView) dialogView.findViewById(R.id.tv_title);
        TextView content = (TextView) dialogView.findViewById(R.id.tv_error_content);

        title.setText(name);
        content.setText(data);

        final android.app.AlertDialog alertDialog = dialogBuilder.create();

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void onHaveTicket() {
        mNoTicket.setVisibility(View.GONE);
    }

    @Override
    public void onNoTicket() {
        mNoTicket.setVisibility(View.VISIBLE);
        mNoTicket.setText("Không có vận đơn lỗi");
    }
}
