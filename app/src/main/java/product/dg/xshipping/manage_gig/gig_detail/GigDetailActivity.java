package product.dg.xshipping.manage_gig.gig_detail;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Rating;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.model.Shipper;
import product.dg.xshipping.util.FS;

public class GigDetailActivity extends AppCompatActivity implements GigDetailInterfaces.RequiredViewOps {
    private GigDetailInterfaces.ProvidedPresenterOps mPresenter;
    ProgressDialog mProgressDialog;
    String mSender;

    TextView mTitle;
    int mCurrentGigID;

    TextView mReceiverName;
    TextView mReceiverPhone;
    TextView mReceiverAddress;
    TextView mReceiverCity;
    TextView mReceiverDistrict;
    TextView mReceiverWard;

    TextView mGigName;
    TextView mGigWeight;
    TextView mGigPaymentMethod;
    TextView mGigSize;
    TextView mGigDate;
    TextView mGigCod;
    TextView mGigNote;

    TextView mSenderName;
    TextView mSenderPhone;
    TextView mSenderAddress;
    TextView mSenderBankName;
    TextView mSenderBankNumber;
    TextView mSenderBankBank;

    MenuItem mRatingMenu;

    LinearLayout mllConfirmImage;
    ImageView mConfirmImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gig_detail);

        Intent mIntent = getIntent();
        mCurrentGigID = mIntent.getIntExtra(FS.BUNDLE_GIG_ID, 0);
        mSender = mIntent.getStringExtra(FS.BUNDLE_GIG_SENDER);

        bindingControls();
        setupControlEvents();

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.loadData(mCurrentGigID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_gig_detail_menu, menu);

        MenuItem tracking = (MenuItem) menu.findItem(R.id.action_tracking);
        MenuItem update = (MenuItem) menu.findItem(R.id.action_update);
        MenuItem delete = (MenuItem) menu.findItem(R.id.action_delete);
        MenuItem cancel = (MenuItem) menu.findItem(R.id.action_cancel);
        MenuItem viewShipper = (MenuItem) menu.findItem(R.id.action_view_shipper);
        mRatingMenu = (MenuItem) menu.findItem(R.id.action_rating);

        tracking.setVisible(false);
        update.setVisible(false);
        delete.setVisible(false);
        cancel.setVisible(false);
        viewShipper.setVisible(false);
        mRatingMenu.setVisible(false);

        switch (mSender) {
            case FS.GIG_DONE:
                delete.setVisible(true);
                viewShipper.setVisible(true);
                mRatingMenu.setVisible(true);
                mllConfirmImage.setVisibility(View.VISIBLE);
                break;
            case FS.GIG_ERROR:
                delete.setVisible(true);
                viewShipper.setVisible(true);
                mRatingMenu.setVisible(true);
                break;
            case FS.GIG_PROCESSING:
                tracking.setVisible(true);
                cancel.setVisible(true);
                viewShipper.setVisible(true);
                break;
            case FS.GIG_WAITING:
                update.setVisible(true);
                delete.setVisible(true);
                break;
            case FS.GIG_WAITING_COD:
                mllConfirmImage.setVisibility(View.VISIBLE);
                //delete.setVisible(true);
                viewShipper.setVisible(true);
                mRatingMenu.setVisible(true);
                break;
            case FS.GIG_WAITING_PICK:
                delete.setVisible(true);
                viewShipper.setVisible(true);
                break;
            default:
                break;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_rating: {
                mPresenter.viewShipper();
                //showRatingForm();
                break;
            }
            case R.id.action_view_shipper: {
                mPresenter.viewShipper();
                break;
            }
            case R.id.action_tracking:
                mPresenter.trackingItem();
                break;
            case R.id.action_update:
                mPresenter.updateItem();
                break;
            case R.id.action_delete:
                final AlertDialog dialog = new AlertDialog.Builder(GigDetailActivity.this)
                        .setTitle(getString(R.string.dialog_notice_title))
                        .setMessage(getString(R.string.dialog_message_gig_detail_delete))
                        .setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mPresenter.deleteItem();
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create();
                if (!dialog.isShowing()) {
                    dialog.show();
                }
                break;
            case R.id.action_cancel:
                final AlertDialog dialog_cancel = new AlertDialog.Builder(GigDetailActivity.this)
                        .setTitle(getString(R.string.dialog_notice_title))
                        .setMessage(getString(R.string.dialog_message_gig_detail_cancel))
                        .setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mPresenter.cancelItem();
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create();
                if (!dialog_cancel.isShowing()) {
                    dialog_cancel.show();
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void bindingControls() {
        mTitle = (TextView) findViewById(R.id.tv_title);
        mReceiverName = (TextView) findViewById(R.id.tv_receiver_name);
        mReceiverPhone = (TextView) findViewById(R.id.tv_receiver_phone);
        mReceiverAddress = (TextView) findViewById(R.id.tv_receiver_address);
        mReceiverCity = (TextView) findViewById(R.id.tv_receiver_city);
        mReceiverDistrict = (TextView) findViewById(R.id.tv_receiver_district);
        mReceiverWard = (TextView) findViewById(R.id.tv_receiver_ward);

        mGigName = (TextView) findViewById(R.id.tv_gig_name);
        mGigWeight = (TextView) findViewById(R.id.tv_gig_weight);
        mGigPaymentMethod = (TextView) findViewById(R.id.tv_gig_payment_method);
        mGigSize = (TextView) findViewById(R.id.tv_gig_size);
        mGigDate = (TextView) findViewById(R.id.tv_gig_pickup_date);
        mGigCod = (TextView) findViewById(R.id.tv_gig_cod);
        mGigNote = (TextView) findViewById(R.id.tv_gig_note);

        mSenderName = (TextView) findViewById(R.id.tv_sender_name);
        mSenderPhone = (TextView) findViewById(R.id.tv_sender_phone);
        mSenderAddress = (TextView) findViewById(R.id.tv_sender_address);
        mSenderBankName = (TextView) findViewById(R.id.tv_sender_bank_account_name);
        mSenderBankNumber = (TextView) findViewById(R.id.tv_sender_bank_account_number);
        mSenderBankBank = (TextView) findViewById(R.id.tv_sender_bank_account_bank);

        mllConfirmImage = (LinearLayout) findViewById(R.id.ll_confirm_image);
        mConfirmImage = (ImageView) findViewById(R.id.iv_confirm_image);
    }

    void setupControlEvents() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(null);
        }

        mTitle.setText(getString(R.string.gig_detail_title));
    }

    void init() {
        GigDetailPresenter presenter = new GigDetailPresenter(this);
        GigDetailModel model = new GigDetailModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getActivity() {
        return GigDetailActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(GigDetailActivity.this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void presentData(GigDetailItem data) {
        if (!data.getRate().trim().equalsIgnoreCase("0")) {
            mRatingMenu.setVisible(false);
        }
        setTextFor(mReceiverName, data.getReceiverName());
        setTextFor(mReceiverPhone, data.getReceiverPhone());
        setTextFor(mReceiverAddress, data.getReceiverAddress() + ", " + data.getReceiverWard() + ", " + data.getReceiverDistrict() + ", " + data.getReceiverCity());
        //setTextFor(mReceiverCity, data.getReceiverCity());
        //setTextFor(mReceiverDistrict, data.getReceiverDistrict());
        //setTextFor(mReceiverWard, data.getReceiverWard());

        setTextFor(mGigName, data.getGigName());
        setTextFor(mGigWeight, data.getGigWeight());
        setTextFor(mGigPaymentMethod, data.getGigPaymentMethod());
        setTextFor(mGigSize, data.getGigSize());
        setTextFor(mGigDate, data.getGigDate());
        setTextFor(mGigCod, data.getGigCod());
        setTextFor(mGigNote, data.getGigNote());

        setTextFor(mSenderName, data.getSenderName());
        setTextFor(mSenderPhone, data.getSenderPhone());
        setTextFor(mSenderAddress, data.getSenderAddress());
        setTextFor(mSenderBankName, data.getSenderBankName());
        setTextFor(mSenderBankNumber, data.getSenderBankNumber());
        setTextFor(mSenderBankBank, data.getSenderBankBank());

        if (mllConfirmImage.getVisibility() == View.VISIBLE) {
            Picasso.with(GigDetailActivity.this).load(FS.IMAGE_HOST + "uploadedimages/" + data.getID() + "_success_confirm.jpg").into(mConfirmImage);
        }
    }

    @Override
    public void presentShipper(final Shipper data) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(GigDetailActivity.this);
        LayoutInflater inflater = GigDetailActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_shipper_info, null);
        dialogBuilder.setView(dialogView);

        TextView name = (TextView) dialogView.findViewById(R.id.tv_name);
        TextView phone = (TextView) dialogView.findViewById(R.id.tv_phone);
        TextView email = (TextView) dialogView.findViewById(R.id.tv_email);
        final RatingBar ratingBar = (RatingBar) dialogView.findViewById(R.id.rating);
        Button submit = (Button) dialogView.findViewById(R.id.btn_submit);
        RatingBar curRating = (RatingBar) dialogView.findViewById(R.id.rb_cur_rating);
        TextView totalRating = (TextView) dialogView.findViewById(R.id.tv_total_rating);
        ImageView avatar = (ImageView) dialogView.findViewById(R.id.iv_avatar);
        LinearLayout ratingContainer = (LinearLayout) dialogView.findViewById(R.id.ll_rating_container);
        Button call = (Button) dialogView.findViewById(R.id.btn_call);

        if (data == null) {
            return;
        }

        if (!mRatingMenu.isVisible()) {
            ratingContainer.setVisibility(View.GONE);
        }

        if (!data.getAvatar().trim().equalsIgnoreCase("")) {
            Picasso.with(getApplicationContext()).load(FS.IMAGE_HOST + data.getAvatar()).into(avatar);
        }

        curRating.setRating(Float.parseFloat(data.getCurRating()));
        totalRating.setText(data.getTotalRating() + " Ratings");

        name.setText(data.getName());
        phone.setText(data.getPhone());
        email.setText(data.getEmail());

        final android.app.AlertDialog alertDialog = dialogBuilder.create();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rate = (int) ratingBar.getRating();
                mPresenter.rating(Integer.toString(rate));
                alertDialog.dismiss();
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + data.getPhone()));
                if (ActivityCompat.checkSelfPermission(GigDetailActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                GigDetailActivity.this.startActivity(callIntent);
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    void showRatingForm() {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(GigDetailActivity.this);
        LayoutInflater inflater = GigDetailActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_rating, null);
        dialogBuilder.setView(dialogView);

        final RatingBar ratingBar = (RatingBar) dialogView.findViewById(R.id.rating);
        Button submit = (Button) dialogView.findViewById(R.id.btn_submit);

        final android.app.AlertDialog alertDialog = dialogBuilder.create();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rate = (int) ratingBar.getRating();
                mPresenter.rating(Integer.toString(rate));
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void ratingCompleted() {
        showDialog(R.string.rating_title, R.string.rating_success);
    }


    void setTextFor(TextView textView, String data) {
        if (data.trim().equalsIgnoreCase("")) {
            textView.setVisibility(View.GONE);
        }
        else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(data);
        }
    }
}
