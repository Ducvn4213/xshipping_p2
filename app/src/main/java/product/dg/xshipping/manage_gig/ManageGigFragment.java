package product.dg.xshipping.manage_gig;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import product.dg.xshipping.R;
import product.dg.xshipping.manage_gig.completed.done.GigDoneFragment;
import product.dg.xshipping.manage_gig.completed.error.GigErrorFragment;
import product.dg.xshipping.manage_gig.processing.GigProcessingFragment;
import product.dg.xshipping.manage_gig.wait.WaitGigFragment;
import product.dg.xshipping.manage_gig.wait.waiting.GigWaitingFragment;
import product.dg.xshipping.manage_gig.wait.waiting_book_confirm.GigWaitingBookConfirmFragment;
import product.dg.xshipping.manage_gig.completed.waiting_cod.GigWaitingCodFragment;
import product.dg.xshipping.manage_gig.pick.waiting_pick.GigWaitingPickFragment;
import product.dg.xshipping.manage_gig.pick.waiting_pick_confirm.GigWaitingPickConfirmFragment;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.util.FS;

public class ManageGigFragment extends Fragment implements ViewPager.OnPageChangeListener {

    SmartTabLayout mTab;
    ViewPager mViewPager;

    FragmentPagerItemAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_gig, container, false);

        bindingUI(view);
        setupUIData();
        setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            GigProcessingFragment fragment = (GigProcessingFragment) mAdapter.getPage(0);
            if (fragment != null) {
                fragment.onResume();
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        //FSService service = FSService.getInstance();
        //mViewPager.setCurrentItem(service.getSubIndex());
        //service.setSubIndex(0);
    }

    private void bindingUI(View view) {
        mTab = (SmartTabLayout) view.findViewById(R.id.stl_tab);
        mViewPager = (ViewPager) view.findViewById(R.id.vp_container);
    }

    private void setupUIData() {
        FragmentPagerItems.Creator creator = FragmentPagerItems.with(getContext());

        Bundle processing = new Bundle();
        processing.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_PROCESSING);
        creator.add(R.string.manage_gigs_processing, GigProcessingFragment.class, processing);

        mAdapter = new FragmentPagerItemAdapter(getChildFragmentManager(),
                creator.create());

        mViewPager.setAdapter(mAdapter);
        mTab.setCustomTabView(R.layout.layout_smart_tab, R.id.tv_ticket_tab_item);
        mTab.setViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(this);
    }

    private void setupControlEvents() {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void goToTab(int index) {
        mViewPager.setCurrentItem(index);
    }
}
