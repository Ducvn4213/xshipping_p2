package product.dg.xshipping.manage_gig.completed.error;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipping.adapter.GigAdapter;
import product.dg.xshipping.adapter.model.GigItem;

public class GigErrorInterfaces {

    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void updateAdapter(GigAdapter adapter);
        void presentErrorTicket(String name, String data);

        void onNoTicket();
        void onHaveTicket();
    }

    interface ProvidedPresenterOps {
        void loadData();
        void goToDetail(int position);
        void showError(int position);
        GigItem getData(int pos);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigAdapter adapter);
        void updateNewData(List<GigItem> data);
        GigAdapter createAdapterWithData(List<GigItem> data);
        void presentErrorTicket(String name, String data);

        void onNoTicket();
        void onHaveTicket();
    }

    interface ProvidedModelOps {
        void downloadData();
        void downloadError(int position);
        int getIdFromPosition(int position);
        void updateNewData(List<GigItem> data);
        GigItem getData(int pos);
    }
}
