package product.dg.xshipping.manage_gig.edit_gig;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigDetailItem;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.TrackerGPS;
import product.dg.xshipping.util.FS;
import product.dg.xshipping.util.Utils;

public class EditGigActivity extends AppCompatActivity implements EditGigInterfaces.RequiredViewOps, CalendarDatePickerDialogFragment.OnDateSetListener {
    private EditGigInterfaces.ProvidedPresenterOps mPresenter;
    ProgressDialog mProgressDialog;
    private DateTimeFormatter mFormatter;

    GigDetailItem mData;

    TextView mTitle;

    EditText mReceiverName;
    EditText mReceiverPhone;
    EditText mReceiverAddress;
    AppCompatSpinner mReceiverCity;
    AppCompatSpinner mReceiverDistrict;
    AppCompatSpinner mReceiverWard;

    EditText mGigName;
    EditText mGigWeight;
    AppCompatSpinner mGigPaymentMethod;
    AppCompatSpinner mGigSize;
    Button mGigDate;
    AppCompatSpinner mGigCOD;
    EditText mGigCod;
    EditText mGigNote;

    TextView mBankInfo;
    TextView mBankInfo2;
    TextView mSenderName;
    TextView mSenderPhone;
    AppCompatSpinner mSenderAddressSpinner;
    EditText mSenderAddress;
    EditText mSenderBankName;
    EditText mSenderBankNumber;
    EditText mSenderBankBank;

    Button mBack;
    Button mSave;

    List<String> mPaymentMethodList = new ArrayList<>();
    List<String> mGigSizeList = new ArrayList<>();
    List<String> mCODList = new ArrayList<>();
    List<String> mAddressSpinnerList = new ArrayList<>();

    boolean mPreventProvince;
    boolean mPreventDistrict;
    boolean mPreventAddress;
    boolean mPreventResume;

    TrackerGPS mTrackerGPS;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gig_detail_edit);
        mData = (GigDetailItem) getIntent().getSerializableExtra(FS.BUNDLE_GIG_DATA);

        bindingControls();
        setupControlEvents();

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPreventResume) {
            mPreventResume = false;
            return;
        }
        mPresenter.loadData();
        mPresenter.loadGeoData();
    }

    void bindingControls() {
        mTitle = (TextView) findViewById(R.id.tv_title);
        mBankInfo = (TextView) findViewById(R.id.tv_bank_info);
        mBankInfo2 = (TextView) findViewById(R.id.tv_bank_info2);

        mSave = (Button) findViewById(R.id.btn_save);
        mBack = (Button) findViewById(R.id.btn_back);

        mReceiverName = (EditText) findViewById(R.id.edt_receiver_name);
        mReceiverPhone = (EditText) findViewById(R.id.edt_receiver_phone);
        mReceiverAddress = (EditText) findViewById(R.id.edt_receiver_address);
        mReceiverCity = (AppCompatSpinner) findViewById(R.id.spn_province);
        mReceiverDistrict = (AppCompatSpinner) findViewById(R.id.spn_district);
        mReceiverWard = (AppCompatSpinner) findViewById(R.id.spn_ward);

        mGigName = (EditText) findViewById(R.id.edt_gig_name);
        mGigWeight = (EditText) findViewById(R.id.edt_weight);
        mGigPaymentMethod = (AppCompatSpinner) findViewById(R.id.spn_payment_method);
        mGigSize = (AppCompatSpinner) findViewById(R.id.spn_gigs_size);
        mGigDate = (Button) findViewById(R.id.btn_pickup_date);
        mGigCOD = (AppCompatSpinner) findViewById(R.id.spn_cod);
        mGigCod = (EditText) findViewById(R.id.edt_cod);
        mGigNote = (EditText) findViewById(R.id.edt_note);

        mSenderName = (TextView) findViewById(R.id.edt_sender_name);
        mSenderPhone = (TextView) findViewById(R.id.edt_sender_phone);
        mSenderAddressSpinner = (AppCompatSpinner) findViewById(R.id.spn_address);
        mSenderAddress = (EditText) findViewById(R.id.edt_address);
        mSenderBankName = (EditText)findViewById(R.id.edt_sender_bank_account_name);
        mSenderBankNumber= (EditText)findViewById(R.id.edt_sender_bank_account_number);
        mSenderBankBank = (EditText)findViewById(R.id.edt_sender_bank_account_bank);
    }

    void setupControlEvents() {
        mBankInfo.setVisibility(View.VISIBLE);
        mBankInfo2.setVisibility(View.VISIBLE);

        mSenderBankName.setVisibility(View.VISIBLE);
        mSenderBankNumber.setVisibility(View.VISIBLE);
        mSenderBankBank.setVisibility(View.VISIBLE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(null);
        }

        mTitle.setText(getString(R.string.gig_edit_title));

        mSenderAddressSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mPreventAddress) {
                    mPreventAddress = false;
                    return;
                }
                if (i == 0) {
                    User currentUser = FSService.getInstance().isFacebookLogin() ? FSService.getInstance().getCurrentFacebookUser() : FSService.getInstance().getCurrentUser();
                    if (currentUser != null) {
                        mSenderAddress.setText(currentUser.getAddress());
                    }
                }
                else if (i == 1){
                    checkLocationPermission();
                }
                else {
                    try {
                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                        startActivityForResult(builder.build(getActivity()), FS.PICK_LOCATION_REQUEST_CODE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO
            }
        });

        mGigCOD.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    mGigCod.setText("");
                    mGigCod.setEnabled(false);
                    disablePaymentInfo();
                }
                else {
                    mGigCod.setEnabled(true);
                    mGigCod.requestFocus();
                    enablePaymentInfo();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog dialog = new AlertDialog.Builder(EditGigActivity.this)
                        .setTitle(getString(R.string.dialog_notice_title))
                        .setMessage(getString(R.string.dialog_message_gig_edit_end_edit))
                        .setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mPresenter.endEndEdit();
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create();
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            }
        });

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String receiverName = mReceiverName.getText().toString();
                String receiverPhone = mReceiverPhone.getText().toString();
                String receiverAddress = mReceiverAddress.getText().toString();

                String receiverProvince = mReceiverCity.getSelectedItem().toString();
                String receiverDistrict = mReceiverDistrict.getSelectedItem().toString();
                String receiverWard = mReceiverWard.getSelectedItem().toString();

                if (mReceiverCity.getSelectedItemPosition() == 0) {
                    showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_province_missing);
                    return;
                }

                if (mReceiverDistrict.getSelectedItemPosition() == 0) {
                    showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_district_missing);
                    return;
                }

                if (mReceiverWard.getSelectedItemPosition() == 0) {
                    showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_ward_missing);
                    return;
                }

                String gigName = mGigName.getText().toString();
                String gigWeight = mGigWeight.getText().toString();
                String gigPaymentMethod = mGigPaymentMethod.getSelectedItem().toString();
                String gigSize = mGigSize.getSelectedItem().toString();
                String gigDate = mGigDate.getText().toString();
                String gigCod = mGigCod.getText().toString();
                String gigNote = mGigNote.getText().toString();

                String senderName = mSenderName.getText().toString();
                String senderPhone = mSenderPhone.getText().toString();
                String senderAddress = mSenderAddress.getText().toString();
                String senderBankName = mSenderBankName.getText().toString();
                String senderBankNumber = mSenderBankNumber.getText().toString();
                String senderBankBank = mSenderBankBank.getText().toString();

                mPresenter.updateGigTicket(receiverName, receiverPhone, receiverAddress,
                        receiverProvince, receiverDistrict, receiverWard, senderAddress,
                        gigName, gigWeight, gigPaymentMethod, gigSize, gigDate, gigCod,
                        gigNote, senderBankName, senderBankNumber, senderBankBank);
            }
        });

        mReceiverCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mPreventProvince) {
                    mPreventProvince = false;
                    return;
                }
                mPresenter.onSelectProvince(i);
                mReceiverDistrict.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO Do nothing
            }
        });

        mReceiverDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mPreventDistrict) {
                    mPreventDistrict = false;
                    return;
                }
                mPresenter.onSelectDistrict(i);
                mReceiverWard.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO Do nothing
            }
        });

        mGigDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar start = Calendar.getInstance();
                DateTime date = (new DateTime()).withTime(0, 0, 0, 0);
                start.setTimeInMillis(date.getMillis());

                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(EditGigActivity.this)
                        .setFirstDayOfWeek(Calendar.MONDAY)
                        .setPreselectedDate(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH))
                        .setDoneText(getString(R.string.dialog_button_ok))
                        .setCancelText(getString(R.string.dialog_button_cancel))
                        .setThemeLight();
                cdp.show(getSupportFragmentManager(), FS.DATE_PICKER_START_DIALOG_TAG);
            }
        });
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        if (dialog.getTag().equalsIgnoreCase(FS.DATE_PICKER_START_DIALOG_TAG)) {
            DateTime dateTime = (new DateTime()).withTime(0, 0, 0, 0);
            dateTime = dateTime.withDate(year, monthOfYear + 1, dayOfMonth);

            DateTime newDateTime = new DateTime();
            newDateTime = newDateTime.withTime(0, 0, 0, 0);
            if ((newDateTime.getMillis() - dateTime.getMillis()) > 0) {
                showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_date_wrong);
                return;
            }

            newDateTime = new DateTime();
            if (newDateTime.getHourOfDay() >= 16) {
                newDateTime = newDateTime.withTime(0, 0, 0, 0);
                if ((newDateTime.getMillis() - dateTime.getMillis()) == 0) {
                    showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_date_late);
                    return;
                }
            }

            String prefix = getString(R.string.send_gigs_pickup_date);
            mGigDate.setText(prefix + ": " + mFormatter.print(dateTime.getMillis()));
        }
    }

    private void checkLocationPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        else {
            doFillCurrentAddress();
            return;
        }

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        else {
            doFillCurrentAddress();
            return;
        }

        String[] perArr = new String[permissions.size()];
        perArr = permissions.toArray(perArr);
        ActivityCompat.requestPermissions(getActivity(), perArr, FS.PERMISSIONS_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FS.PICK_LOCATION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String addressString = PlacePicker.getPlace(data, getActivity()).getAddress().toString();
                mSenderAddress.setText(addressString);
                mPreventResume = true;
            }
        }
    }

    private void doFillCurrentAddress() {
        mTrackerGPS = new TrackerGPS(getActivity());
        if(mTrackerGPS.canGetLocation()){

            double longitude = mTrackerGPS.getLongitude();
            double latitude = mTrackerGPS.getLatitude();

            if (longitude == 0 && latitude == 0) {
                doFillCurrentAddress();
                return;
            }

            Geocoder geocoder = new Geocoder(getActivity());
            try {
                Address address = geocoder.getFromLocation(latitude, longitude, 1).get(0);
                String addressString = Utils.getStringAddressFromAddress(address);
                mSenderAddress.setText(addressString);
            } catch (IOException e) {
                showDialog(R.string.dialog_title_error, R.string.dialog_message_get_current_location_fail);
            }
        }
        else
        {
            mTrackerGPS.showSettingsAlert();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == FS.PERMISSIONS_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    doFillCurrentAddress();
                }
                else {
                    showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_location_permission);
                }
            }
            else {
                showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_location_permission);
            }
        }
    }

    void disablePaymentInfo() {
        mBankInfo.setVisibility(View.GONE);
        mBankInfo2.setVisibility(View.GONE);

        mSenderBankName.setVisibility(View.GONE);
        mSenderBankNumber.setVisibility(View.GONE);
        mSenderBankBank.setVisibility(View.GONE);
    }

    void enablePaymentInfo() {
        mBankInfo.setVisibility(View.VISIBLE);
        mBankInfo2.setVisibility(View.VISIBLE);
        mSenderBankName.setVisibility(View.VISIBLE);
        mSenderBankNumber.setVisibility(View.VISIBLE);
        mSenderBankBank.setVisibility(View.VISIBLE);
    }

    void init() {
        Utils.hideKeyboard(getActivity());

        mFormatter = DateTimeFormat.forPattern(getString(R.string.date_pattern));

        EditGigPresenter presenter = new EditGigPresenter(this);
        EditGigModel model = new EditGigModel(presenter);
        model.setData(mData);

        presenter.setModel(model);

        mPresenter = presenter;

        mPresenter.loadGeoData();

        //Payment Method
        String[] items = getResources().getStringArray(R.array.array_payment_method);
        if (mPaymentMethodList.size() == 0) {
            Collections.addAll(mPaymentMethodList, items);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item,
                R.id.tv_item_text, mPaymentMethodList);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mGigPaymentMethod.setAdapter(pm_adapter);

        //Gig Size
        items = getResources().getStringArray(R.array.array_gig_size);
        if (mGigSizeList.size() == 0) {
            Collections.addAll(mGigSizeList, items);
        }

        ArrayAdapter<String> gs_adapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item,
                R.id.tv_item_text, mGigSizeList);
        gs_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mGigSize.setAdapter(gs_adapter);

        //Cod
        items = getResources().getStringArray(R.array.array_cod);
        if (mCODList.size() == 0) {
            Collections.addAll(mCODList, items);
        }

        ArrayAdapter<String> cod_adapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item,
                R.id.tv_item_text, mCODList);
        cod_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mGigCOD.setAdapter(cod_adapter);

        //Address Spinner
        items = getResources().getStringArray(R.array.array_address);
        if (mAddressSpinnerList.size() == 0) {
            Collections.addAll(mAddressSpinnerList, items);
        }

        ArrayAdapter<String> a_adapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item,
                R.id.tv_item_text, mAddressSpinnerList);
        a_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mPreventAddress = true;
        mSenderAddressSpinner.setAdapter(a_adapter);
    }

    @Override
    public AppCompatActivity getActivity() {
        return EditGigActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(EditGigActivity.this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void presentData(GigDetailItem data) {
        mReceiverName.setText(data.getReceiverName());
        mReceiverPhone.setText(data.getReceiverPhone());
        mReceiverAddress.setText(data.getReceiverAddress());

        mGigName.setText(data.getGigName());
        mGigWeight.setText(data.getGigWeight());
        mGigPaymentMethod.setSelection(Utils.getPositionByName(mPaymentMethodList, data.getGigPaymentMethod()));
        mGigSize.setSelection(Utils.getPositionByName(mGigSizeList, data.getGigSize()));
        mGigDate.setText(data.getGigDate());

        if (data.getGigCod().trim().equalsIgnoreCase("")) {
            mGigCOD.setSelection(0);
        }
        else {
            mGigCOD.setSelection(1);
        }

        mGigCod.setText(data.getGigCod());
        mGigNote.setText(data.getGigNote());

        mSenderName.setText(data.getSenderName());
        mSenderPhone.setText(data.getSenderPhone());
        mSenderAddress.setText(data.getSenderAddress());
        mSenderBankName.setText(data.getSenderBankName());
        mSenderBankNumber.setText(data.getSenderBankNumber());
        mSenderBankBank.setText(data.getSenderBankBank());
    }

    @Override
    public void geoDataCallback(ArrayAdapter<String> provinceAdapter, ArrayAdapter<String> districtAdapter, ArrayAdapter<String> wardAdapter) {
        mPreventProvince = true;
        mPreventDistrict = true;

        mReceiverCity.setAdapter(provinceAdapter);
        mReceiverDistrict.setAdapter(districtAdapter);
        mReceiverWard.setAdapter(wardAdapter);
    }

    @Override
    public void geoDataIndexCallback(int province, int district, int ward) {
        mPreventProvince = true;
        mPreventDistrict = true;

        mReceiverCity.setSelection(province);
        mReceiverDistrict.setSelection(district);
        mReceiverWard.setSelection(ward);
    }
}

