package product.dg.xshipping;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.adapter.model.GigItem;
import product.dg.xshipping.adapter.model.GigItemList;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;
import product.dg.xshipping.util.Utils;

public class MainModel implements MainInterfaces.ProvidedModelOps {

    private MainInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    MainModel(MainInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void doLogin(final String username, final String password) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_username", username));
        paramList.add(new Param("_password", password));

        mService.login(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                mService.setCurrentUser(data);
                mPresenter.onLoginSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void doLoginFacebook(String id,final String email,final String name) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_faceid", id));

        mService.loginFacebook(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                data.setName(name);
                data.setEmail(email);
                mService.setCurrentFacebookUser(data);
                mPresenter.onLoginSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void checkOldTicket() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_date", Utils.yesterday()));

        mService.getOldTicket(paramList, new FSService.Callback<GigItemList>() {
            @Override
            public void onSuccess(GigItemList data) {
                mPresenter.checkingWithData(data.getData());
            }

            @Override
            public void onFail(String error) {
                //mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void pushThat(List<GigItem> data) {
        String ids = "";
        for (GigItem d : data) {
            ids += d.getId() + "-";
        }

        ids += "0";

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_ids", ids));

        mService.pushThat(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                //TODO
            }

            @Override
            public void onFail(String error) {
                //TODO
            }
        });
    }

    @Override
    public void cancelThat(List<GigItem> data) {
        String ids = "";
        for (GigItem d : data) {
            ids += d.getId() + "-";
        }

        ids += "0";

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_ids", ids));

        mService.cancelThat(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                //TODO
            }

            @Override
            public void onFail(String error) {
                //TODO
            }
        });
    }
}
