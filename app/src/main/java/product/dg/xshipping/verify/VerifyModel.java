package product.dg.xshipping.verify;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class VerifyModel implements VerifyInterfaces.ProvidedModelOps {
    private VerifyInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    VerifyModel(VerifyInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void doVerify(String id,final String phone) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_id", id));
        paramList.add(new Param("_phone", phone));

        mService.verify(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                if (mService.getCurrentFacebookUser() != null) {
                    mService.getCurrentFacebookUser().setPhone(phone);
                    mService.getCurrentFacebookUser().setIsActive("1");
                }
                else {
                    mService.getCurrentUser().setPhone(phone);
                    mService.getCurrentUser().setIsActive("1");
                }
                mPresenter.onVerifySuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.verify_fail);
            }
        });
    }
}
