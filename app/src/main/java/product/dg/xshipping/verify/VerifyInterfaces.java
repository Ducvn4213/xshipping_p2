package product.dg.xshipping.verify;

import android.support.v7.app.AppCompatActivity;

public class VerifyInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void doVerify(String id, String phone);
        void openMain();
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void onVerifySuccess();
    }

    interface ProvidedModelOps {
        void doVerify(String id, String phone);
    }
}
