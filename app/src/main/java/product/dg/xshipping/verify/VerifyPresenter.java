package product.dg.xshipping.verify;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;

import product.dg.xshipping.MainActivity;

public class VerifyPresenter implements VerifyInterfaces.ProvidedPresenterOps, VerifyInterfaces.RequiredPresenterOps {
    private WeakReference<VerifyInterfaces.RequiredViewOps> mView;
    private VerifyInterfaces.ProvidedModelOps mModel;

    VerifyPresenter(VerifyInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(VerifyInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(VerifyInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void doVerify(String id, String phone) {
        mModel.doVerify(id, phone);
    }

    @Override
    public void openMain() {
        mView.get().getActivity().finish();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void onVerifySuccess() {
        mView.get().hideWaiting();
        openMain();
    }
}
