package product.dg.xshipping.verify;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;

import product.dg.xshipping.R;
import product.dg.xshipping.login.LoginActivity;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.util.FS;

public class VerifyActivity extends AppCompatActivity implements VerifyInterfaces.RequiredViewOps{

    Button mVerify;
    Button mCancel;

    ProgressDialog mProgressDialog;

    private VerifyInterfaces.ProvidedPresenterOps mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AccountKit.initialize(getApplicationContext());
        setContentView(R.layout.activity_verify_phone);

        init();

        bindingControl();
        setupControlEvents();
    }

    private void init() {
        VerifyPresenter presenter = new VerifyPresenter(this);
        VerifyModel model = new VerifyModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    private void bindingControl() {
        mVerify = (Button) findViewById(R.id.btn_verify);
        mCancel = (Button) findViewById(R.id.btn_cancel);
    }

    private void setupControlEvents() {
        mVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLoginPhone(null);
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VerifyActivity.this.finish();
            }
        });
    }

    public void onLoginPhone(final View view) {
        final Intent intent = new Intent(VerifyActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);

        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, FS.FACEBOOK_SMS_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FS.FACEBOOK_SMS_REQUEST_CODE) {
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);

            if (loginResult.getError() != null) {
                VerifyActivity.this.showDialog(R.string.dialog_title_error, R.string.verify_fail);
                return;
            } else if (loginResult.wasCancelled()) {
                VerifyActivity.this.showDialog(R.string.dialog_title_error, R.string.verify_fail);
                return;
            } else {
                VerifyActivity.this.onVerifySuccess();
            }
        }
    }

    private void onVerifySuccess() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {

                PhoneNumber phoneNumber = account.getPhoneNumber();
                String phoneNumberString = phoneNumber.toString();

                User currentAccount = FSService.getInstance().getCurrentUser();
                if (currentAccount == null) {
                    currentAccount = FSService.getInstance().getCurrentFacebookUser();
                }
                String accountID = currentAccount.getAccountID();
                mPresenter.doVerify(accountID, phoneNumberString);
            }

            @Override
            public void onError(final AccountKitError error) {
                VerifyActivity.this.showDialog(R.string.dialog_title_error, R.string.verify_fail);
                return;
            }
        });
    }

    @Override
    public AppCompatActivity getActivity() {
        return VerifyActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(VerifyActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(VerifyActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
