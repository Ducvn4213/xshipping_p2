package product.dg.xshipping.statistic.statistic_detail;


import java.lang.ref.WeakReference;

import product.dg.xshipping.R;

public class StatisticDetailPresenter implements StatisticDetailInterfaces.ProvidedPresenterOps, StatisticDetailInterfaces.RequiredPresenterOps{
    private WeakReference<StatisticDetailInterfaces.RequiredViewOps> mView;
    private StatisticDetailInterfaces.ProvidedModelOps mModel;

    StatisticDetailPresenter(StatisticDetailInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }


    @Override
    public void setView(StatisticDetailInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(StatisticDetailInterfaces.ProvidedModelOps model) {
        mModel = model;
    }


    @Override
    public void loadData(String from, String to) {
        mModel.loadData(from, to);
    }

    @Override
    public void showDialog(final int title, final int message) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title, final String message) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void requestPresent() {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().requestPresent();
            }
        });
    }
}
