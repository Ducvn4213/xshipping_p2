package product.dg.xshipping.statistic.statistic_detail_count;

        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ListView;
        import android.widget.TextView;

        import java.util.ArrayList;
        import java.util.List;

        import product.dg.xshipping.R;
        import product.dg.xshipping.adapter.StatisticAdapter;
        import product.dg.xshipping.adapter.model.StatisticItem;
        import product.dg.xshipping.model.StatisticCount;
        import product.dg.xshipping.statistic.statistic_detail.StatisticDetailModel;

public class StatisticCountFragment extends Fragment {

    TextView intro1;
    TextView intro2;
    ListView mListview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistic_count, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    void bindingControls(View view) {
        intro1 = (TextView) view.findViewById(R.id.intro1);
        intro2 = (TextView) view.findViewById(R.id.intro2);
        mListview = (ListView) view.findViewById(R.id.lv_statistic);
    }

    @Override
    public void onResume() {
        super.onResume();
        StatisticDetailModel parentModel = StatisticDetailModel.getInstance();
        if (parentModel == null) {
            return;
        }

        List<StatisticCount> data = parentModel.getCountReport();

        if (data == null) {
            return;
        }

        String from = parentModel.getFrom();
        String to = parentModel.getTo();

        intro1.setText("Từ ngày: " + from + " đến ngày "+ to);
        intro2.setText("Tổng: " + data.size() + " đơn hàng");
        StatisticAdapter adapter = new StatisticAdapter(StatisticCountFragment.this.getActivity(), convertToItem(data));
        mListview.setAdapter(adapter);
    }

    List<StatisticItem> convertToItem(List<StatisticCount> data) {
        List<StatisticItem> result = new ArrayList<>();

        for (StatisticCount d : data) {
            StatisticItem check = isExistDate(d, result);
            if (check == null) {
                StatisticItem newone = new StatisticItem();
                newone.setDate(d.getCreateDate());
                newone.setValue("1");
                result.add(newone);
            }
            else {
                int old = Integer.parseInt(check.getValue());
                check.setValue(Integer.toString(old + 1));
            }
        }

        return result;
    }

    StatisticItem isExistDate(StatisticCount d, List<StatisticItem> collection) {
        for (StatisticItem item : collection) {
            if (item.getDate().equalsIgnoreCase(d.getCreateDate())) {
                return item;
            }
        }

        return null;
    }

    void setupControlEvents() {}

    void init() {}
}
