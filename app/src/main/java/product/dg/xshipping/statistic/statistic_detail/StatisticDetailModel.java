package product.dg.xshipping.statistic.statistic_detail;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.Statistic;
import product.dg.xshipping.model.StatisticCount;
import product.dg.xshipping.model.StatisticIncome;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class StatisticDetailModel implements StatisticDetailInterfaces.ProvidedModelOps {
    private StatisticDetailInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();
    Statistic mData;
    String mFrom;
    String mTo;

    StatisticDetailModel(StatisticDetailInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
        mInstance = this;
    }

    private static StatisticDetailModel mInstance;
    public static StatisticDetailModel getInstance() {
        return mInstance;
    }

    @Override
    public void loadData(String from, String to) {
        mFrom = from;
        mTo = to;
        String newFrom = correctFormat(from);
        String newTo = correctFormat(to);

        List<Param> params = new ArrayList<>();
        params.add(new Param("_from", newFrom));
        params.add(new Param("_to", newTo));

        mService.loadStatistic(params, new FSService.Callback<Statistic>() {
            @Override
            public void onSuccess(Statistic data) {
                mData = data;
                mPresenter.requestPresent();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, "Không có dữ liệu để thống kê");
            }
        });
    }

    String correctFormat(String data) {
        String[] dateSplitted = data.split("-");
        return dateSplitted[2] + "-" + dateSplitted[1] + "-" + dateSplitted[0];
    }

    public List<StatisticIncome> getIncomeReport() {
        if (mData == null) {
            return null;
        }
        return mData.getDataIncome();
    }

    public List<StatisticCount> getCountReport() {
        if (mData == null) {
            return null;
        }
        return mData.getDataCount();
    }

    public String getFrom() {
        return mFrom;
    }

    public String getTo() {
        return mTo;
    }
}
