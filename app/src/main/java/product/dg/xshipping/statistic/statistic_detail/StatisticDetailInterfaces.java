package product.dg.xshipping.statistic.statistic_detail;

import android.support.v7.app.AppCompatActivity;

public class StatisticDetailInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void requestPresent();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void loadData(String from, String to);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void requestPresent();
    }

    interface ProvidedModelOps {
        void loadData(String from, String to);
    }
}
