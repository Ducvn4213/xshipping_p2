package product.dg.xshipping.statistic.statistic_detail_income;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.StatisticAdapter;
import product.dg.xshipping.adapter.model.Statistic;
import product.dg.xshipping.adapter.model.StatisticItem;
import product.dg.xshipping.model.StatisticIncome;
import product.dg.xshipping.statistic.statistic_detail.StatisticDetailModel;

public class StatisticIncomeFragment extends Fragment {

    TextView intro1;
    TextView intro2;
    ListView mListview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistic_income, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    void bindingControls(View view) {
        intro1 = (TextView) view.findViewById(R.id.intro1);
        intro2 = (TextView) view.findViewById(R.id.intro2);
        mListview = (ListView) view.findViewById(R.id.lv_statistic);
    }

    @Override
    public void onResume() {
        super.onResume();
        StatisticDetailModel parentModel = StatisticDetailModel.getInstance();
        if (parentModel == null) {
            return;
        }

        List<StatisticIncome> data = parentModel.getIncomeReport();

        if (data == null) {
            return;
        }

        correctDate(data);

        String from = parentModel.getFrom();
        String to = parentModel.getTo();

        intro1.setText("Từ ngày: " + from + " đến ngày "+ to);
        intro2.setText("Tổng: " + getTotalMoney(data) + " VNĐ");
        StatisticAdapter adapter = new StatisticAdapter(StatisticIncomeFragment.this.getActivity(), convertToItem(data));
        mListview.setAdapter(adapter);
    }

    int getTotalMoney(List<StatisticIncome> data) {
        int result = 0;
        for (StatisticIncome d : data) {
            result += Integer.parseInt(d.getMoney());
        }

        return result;
    }

    void correctDate(List<StatisticIncome> data) {
        for (StatisticIncome d : data) {
            String old = d.getCreateDate();
            String[] oldSplitted = old.split(" ");
            d.setCreateDate(oldSplitted[0]);
        }
    }

    List<StatisticItem> convertToItem(List<StatisticIncome> data) {
        List<StatisticItem> result = new ArrayList<>();

        for (StatisticIncome d : data) {
            StatisticItem check = isExistDate(d, result);
            if (check == null) {
                StatisticItem newone = new StatisticItem();
                newone.setDate(d.getCreateDate());
                newone.setValue(d.getMoney());
                result.add(newone);
            }
            else {
                int old = Integer.parseInt(check.getValue());
                int _new = old + Integer.parseInt(d.getMoney());
                check.setValue(Integer.toString(old + _new));
            }
        }

        return result;
    }

    StatisticItem isExistDate(StatisticIncome d, List<StatisticItem> collection) {
        for (StatisticItem item : collection) {
            if (item.getDate().equalsIgnoreCase(d.getCreateDate())) {
                return item;
            }
        }

        return null;
    }

    void setupControlEvents() {}

    void init() {}
}
