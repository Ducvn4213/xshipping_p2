package product.dg.xshipping.change_password;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import product.dg.xshipping.R;
import product.dg.xshipping.verify.VerifyActivity;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordInterfaces.RequiredViewOps {

    ProgressDialog mProgressDialog;
    EditText mOldPassword;
    EditText mNewPassword;
    EditText mReNewPassword;
    Button mChange;

    private ChangePasswordInterfaces.ProvidedPresenterOps mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        init();

        bindingControls();
        setupControlEvents();
    }

    private void init() {
        ChangePasswordPresenter presenter = new ChangePasswordPresenter(this);
        ChangePasswordModel model = new ChangePasswordModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    private void bindingControls() {
        mOldPassword = (EditText) findViewById(R.id.edt_oldpassword);
        mNewPassword = (EditText) findViewById(R.id.edt_newpassword);
        mReNewPassword = (EditText) findViewById(R.id.edt_repassword);
        mChange = (Button) findViewById(R.id.btn_change);
    }

    private void setupControlEvents() {
        mChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String oldPassword = mOldPassword.getText().toString();
                String newPassword = mNewPassword.getText().toString();
                String reNewPassword = mReNewPassword.getText().toString();

                mPresenter.doChangePassword(oldPassword, newPassword, reNewPassword);
            }
        });
    }

    @Override
    public AppCompatActivity getActivity() {
        return ChangePasswordActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(ChangePasswordActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(ChangePasswordActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
