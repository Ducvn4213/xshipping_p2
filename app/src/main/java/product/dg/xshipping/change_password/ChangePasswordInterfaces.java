package product.dg.xshipping.change_password;

import android.support.v7.app.AppCompatActivity;

public class ChangePasswordInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void doChangePassword(String password_old, String password_new, String password_renew);
        void openMain();
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void onChangePasswordSuccess();
        void saveUserCredentials(String username, String password);
    }

    interface ProvidedModelOps {
        void doChangePassword(String password_old, String password_new);
    }
}
