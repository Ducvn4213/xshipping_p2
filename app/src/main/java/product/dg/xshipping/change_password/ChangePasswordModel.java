package product.dg.xshipping.change_password;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class ChangePasswordModel implements ChangePasswordInterfaces.ProvidedModelOps {

    private ChangePasswordInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    ChangePasswordModel(ChangePasswordInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void doChangePassword(String password_old, final String password_new) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_oldpassword", password_old));
        paramList.add(new Param("_newpassword", password_new));

        mService.change_password(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                String username = mService.getCurrentUser().getEmail();
                String password = password_new;
                mPresenter.saveUserCredentials(username, password);
                mPresenter.onChangePasswordSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.change_password_fail);
            }
        });
    }
}
