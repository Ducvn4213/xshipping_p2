package product.dg.xshipping;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipping.adapter.model.GigItem;

public class MainPresenter implements MainInterfaces.ProvidedPresenterOps, MainInterfaces.RequiredPresenterOps{
    private WeakReference<MainInterfaces.RequiredViewOps> mView;
    private MainInterfaces.ProvidedModelOps mModel;

    MainPresenter(MainInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(MainInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(MainInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void doLogin(String username, String password) {
        mView.get().showWaiting();
        mModel.doLogin(username, password);
    }

    @Override
    public void doLoginFacebook(String id, String email, String name) {
        mModel.doLoginFacebook(id, email, name);
    }

    @Override
    public void checkOldTicket() {
        mModel.checkOldTicket();
    }

    @Override
    public void pushThat(List<GigItem> data) {
        mModel.pushThat(data);
    }

    @Override
    public void cancelThat(List<GigItem> data) {
        mModel.cancelThat(data);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void onLoginSuccess() {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().loginSuccessCallback();
            }
        });
    }

    @Override
    public void checkingWithData(final List<GigItem> data) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().checkingWithData(data);
            }
        });
    }
}
