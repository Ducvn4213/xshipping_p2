package product.dg.xshipping.tracking_shipper;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.model.Position;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class TrackingShipperModel implements TrackingShipperInterfaces.ProvidedModelOps{
    private TrackingShipperInterfaces.RequiredPresenterOps mPresenter;
    private FSService mService = FSService.getInstance();

    String mCurrentGigID;

    TrackingShipperModel(TrackingShipperInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void loadData() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", String.valueOf(mCurrentGigID)));

        mService.getGigTicketPosition(paramList, new FSService.Callback<Position>() {
            @Override
            public void onSuccess(Position position) {
                mPresenter.presentData(position);
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void setGigID(String gigID) {
        mCurrentGigID = gigID;
    }
}
