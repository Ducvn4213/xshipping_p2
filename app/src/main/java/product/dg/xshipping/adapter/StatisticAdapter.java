package product.dg.xshipping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.StatisticItem;
import product.dg.xshipping.model.NewsCategory;

public class StatisticAdapter extends ArrayAdapter<StatisticItem> {

    private Context mContext;
    private List<StatisticItem> mData;
    private static LayoutInflater mInflater = null;

    public StatisticAdapter(Context context, List<StatisticItem> data) {
        super(context, R.layout.layout_manage_gig_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_statistic_item, null);
        }

        TextView date = (TextView) view.findViewById(R.id.tv_date);
        TextView value = (TextView) view.findViewById(R.id.tv_value);

        StatisticItem data = mData.get(position);

        date.setText(data.getDate());
        value.setText(data.getValue());

        return view;
    }
}
