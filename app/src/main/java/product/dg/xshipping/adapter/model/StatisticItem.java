package product.dg.xshipping.adapter.model;

public class StatisticItem {
    String date;
    String value;

    public StatisticItem() {}

    public String getDate() {
        return date;
    }

    public String getValue() {
        return value;
    }

    public void setDate(String data) {
        this.date = data;
    }

    public void setValue(String data) {
        this.value = data;
    }
}
