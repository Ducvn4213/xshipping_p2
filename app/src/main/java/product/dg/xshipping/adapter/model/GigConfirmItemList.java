package product.dg.xshipping.adapter.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GigConfirmItemList {
    @SerializedName("data")
    List<GigConfirmItem> mData;

    public GigConfirmItemList() {}

    public List<GigConfirmItem> getData() {
        return mData;
    }
}
