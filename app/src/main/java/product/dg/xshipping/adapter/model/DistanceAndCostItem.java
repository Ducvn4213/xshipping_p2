package product.dg.xshipping.adapter.model;

import com.google.gson.annotations.SerializedName;

public class DistanceAndCostItem {
    @SerializedName("distance")
    String mDistance;
    @SerializedName("cost")
    String mCost;

    public DistanceAndCostItem() {}

    public String getDistanse() {
        return mDistance;
    }

    public String getCost() {
        return mCost;
    }
}
