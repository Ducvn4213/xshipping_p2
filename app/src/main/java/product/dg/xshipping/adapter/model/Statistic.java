package product.dg.xshipping.adapter.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import product.dg.xshipping.model.News;
import product.dg.xshipping.model.StatisticCount;
import product.dg.xshipping.model.StatisticIncome;

public class Statistic {
    @SerializedName("count")
    List<StatisticCount> mDataCount;
    @SerializedName("income")
    List<StatisticIncome> mDataIncome;

    public Statistic() {}

    public List<StatisticCount> getDataCount() {
        return mDataCount;
    }
    public List<StatisticIncome> getDataIncome() {
        return mDataIncome;
    }
}
