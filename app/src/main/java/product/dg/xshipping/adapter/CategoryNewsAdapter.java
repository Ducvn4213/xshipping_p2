package product.dg.xshipping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigItem;
import product.dg.xshipping.model.NewsCategory;

public class CategoryNewsAdapter extends ArrayAdapter<NewsCategory> {

    private Context mContext;
    private List<NewsCategory> mData;
    private static LayoutInflater mInflater = null;

    public CategoryNewsAdapter(Context context, List<NewsCategory> data) {
        super(context, R.layout.layout_manage_gig_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_category_news_item, null);
        }

        TextView title = (TextView) view.findViewById(R.id.tv_name);

        NewsCategory data = mData.get(position);

        title.setText(data.getName());

        return view;
    }
}
