package product.dg.xshipping.adapter.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import product.dg.xshipping.model.News;
import product.dg.xshipping.model.NewsCategory;

public class NewsList {
    @SerializedName("data")
    List<News> mData;

    public NewsList() {}

    public List<News> getData() {
        return mData;
    }
}
