package product.dg.xshipping.adapter;

import android.content.Context;
import android.media.Rating;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.GigItem;

public class GigAdapter extends ArrayAdapter<GigItem> {

    private Context mContext;
    private List<GigItem> mData;
    private static LayoutInflater mInflater = null;
    public boolean mIsErrorTicket;

    public GigAdapter(Context context, List<GigItem> data, boolean isErrorTicket) {
        super(context, R.layout.layout_manage_gig_item, data);

        mContext = context;
        mData = data;
        mIsErrorTicket = isErrorTicket;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_manage_gig_item, null);
        }


        TextView title = (TextView) view.findViewById(R.id.edt_name);
        TextView size = (TextView) view.findViewById(R.id.edt_size);
        TextView from = (TextView) view.findViewById(R.id.edt_from);
        TextView to = (TextView) view.findViewById(R.id.edt_to);
        TextView error = (TextView) view.findViewById(R.id.edt_error);
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rb_rating);

        GigItem data = mData.get(position);

        title.setText(data.getTitle());
        size.setText(data.getSize());
        from.setText("Từ: " + data.getFrom());
        to.setText("Đến: " + data.getTo());
        String er = data.getError();
        if (er.trim().equalsIgnoreCase("")) {
            error.setVisibility(View.GONE);
        }
        else {
            error.setVisibility(View.VISIBLE);
            if (mIsErrorTicket) {
                error.setText("Nguyên nhân lỗi: " + data.getError());
            }
            else {
                error.setText("Note từ người giao hàng: " + data.getError());
            }
        }

        int rate = Integer.parseInt(data.getRate());
        if (rate == 0) {
            ratingBar.setVisibility(View.GONE);
        }
        else {
            ratingBar.setVisibility(View.VISIBLE);
            ratingBar.setRating(rate);
        }

        return view;
    }
}
