package product.dg.xshipping.adapter.model;

import com.google.gson.annotations.SerializedName;

public class GigConfirmItem {
    @SerializedName("id")
    int mId;
    @SerializedName("size")
    String mSize;
    @SerializedName("name")
    String mTitle;
    @SerializedName("address")
    String mFrom;
    @SerializedName("address2")
    String mTo;
    @SerializedName("shipper")
    String mShipper;

    public GigConfirmItem() {}

    public void setId(int data) {
        mId = data;
    }
    public void setSize(String data) {
        mSize = data;
    }
    public void setTitle(String data) {
        mTitle = data;
    }
    public void setFrom(String data) {
        mFrom = data;
    }
    public void setTo(String data) {
        mTo = data;
    }
    public void setShipper(String data) {
        mShipper = data;
    }

    public int getId() {
        return mId;
    }
    public String getSize() {
        return mSize;
    }
    public String getTitle() {
        return mTitle;
    }
    public String getFrom() {
        return mFrom;
    }
    public String getTo() {
        return mTo;
    }
    public String getShipper() {
        return mShipper;
    }
}
