package product.dg.xshipping.adapter.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import product.dg.xshipping.model.NewsCategory;

public class NewsCategoryList {
    @SerializedName("data")
    List<NewsCategory> mData;

    public NewsCategoryList() {}

    public List<NewsCategory> getData() {
        return mData;
    }
}
