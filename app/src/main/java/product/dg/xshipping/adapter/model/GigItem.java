package product.dg.xshipping.adapter.model;

import com.google.gson.annotations.SerializedName;

public class GigItem {
    @SerializedName("id")
    int mId;
    @SerializedName("size")
    String mSize;
    @SerializedName("name")
    String mTitle;
    @SerializedName("address")
    String mFrom;
    @SerializedName("address2")
    String mTo;
    @SerializedName("note")
    String mError;
    @SerializedName("rate")
    String mRate;

    public GigItem() {}

    public void setId(int data) {
        mId = data;
    }
    public void setSize(String data) {
        mSize = data;
    }
    public void setTitle(String data) {
        mTitle = data;
    }
    public void setFrom(String data) {
        mFrom = data;
    }
    public void setTo(String data) {
        mTo = data;
    }
    public void setError(String data) {
        mError = data;
    }
    public void setRate(String data) {
        mRate = data;
    }

    public int getId() {
        return mId;
    }
    public String getSize() {
        return mSize;
    }
    public String getTitle() {
        return mTitle;
    }
    public String getFrom() {
        return mFrom;
    }
    public String getTo() {
        return mTo;
    }
    public String getError() {
        return mError;
    }
    public String getRate() {
        return mRate;
    }
}
