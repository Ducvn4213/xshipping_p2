package product.dg.xshipping.send_gig.receiver_info;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.login.LoginActivity;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.util.FS;
import product.dg.xshipping.util.Utils;

import static android.app.Activity.RESULT_OK;


public class ReceiverInfoFragment extends Fragment implements ReceiverInfoInterfaces.RequiredViewOps {

    private ReceiverInfoInterfaces.ProvidedPresenterOps mPresenter;

    Button mNext;
    EditText mName;
    EditText mPhone;
    EditText mAddress;
    AppCompatSpinner mWard;
    AppCompatSpinner mDistrict;
    AppCompatSpinner mProvince;

    boolean mPreventProvince;
    boolean mPreventDistrict;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receiver_info, container, false);

        bindingControls(view);
        setupControlEvents();

        init();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadOldInfo();
        Utils.hideKeyboard(getActivity());
    }

    public void init() {
        ReceiverInfoPresenter presenter = new ReceiverInfoPresenter(this);
        ReceiverInfoModel model = new ReceiverInfoModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    public void bindingControls(View view) {
        mNext = (Button) view.findViewById(R.id.btn_next);
        mName = (EditText) view.findViewById(R.id.edt_receiver_name);
        mPhone = (EditText) view.findViewById(R.id.edt_receiver_phone);
        mAddress = (EditText) view.findViewById(R.id.edt_receiver_address);
        mProvince = (AppCompatSpinner) view.findViewById(R.id.spn_province);
        mDistrict = (AppCompatSpinner) view.findViewById(R.id.spn_district);
        mWard = (AppCompatSpinner) view.findViewById(R.id.spn_ward);
    }

    public void setupControlEvents() {
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mName.getText().toString();
                String phone = mPhone.getText().toString();
                String address = mAddress.getText().toString();
                String province = mProvince.getSelectedItem().toString();
                String district = mDistrict.getSelectedItem().toString();
                String ward = mWard.getSelectedItem().toString();

                if (mProvince.getSelectedItemPosition() == 0) {
                    showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_province_missing);
                    return;
                }

                if (mDistrict.getSelectedItemPosition() == 0) {
                    showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_district_missing);
                    return;
                }

                if (mWard.getSelectedItemPosition() == 0) {
                    showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_ward_missing);
                    return;
                }

                mPresenter.onNext(name, phone, address, province, district, ward);
            }
        });

        mProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mPreventProvince) {
                    mPreventProvince = false;
                    return;
                }
                mPresenter.onSelectProvince(i);
                mDistrict.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO Do nothing
            }
        });

        mDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mPreventDistrict) {
                    mPreventDistrict = false;
                    return;
                }
                mPresenter.onSelectDistrict(i);
                mWard.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO Do nothing
            }
        });
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) ReceiverInfoFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void loadOldData(String name, String phone, String address, ArrayAdapter<String> provinceAdapter, ArrayAdapter<String> districtAdapter, ArrayAdapter<String> wardAdapter, int province, int district, int ward) {
        mName.setText(name);
        mPhone.setText(phone);
        mAddress.setText(address);
        mPreventProvince = true;
        mPreventDistrict = true;
        mWard.setAdapter(wardAdapter);
        mDistrict.setAdapter(districtAdapter);
        mProvince.setAdapter(provinceAdapter);

        mPreventProvince = true;
        mPreventDistrict = true;
        mProvince.setSelection(province);
        mDistrict.setSelection(district);
        mWard.setSelection(ward);
    }

    @Override
    public void loadGeoData(ArrayAdapter<String> provinces, ArrayAdapter<String> districts, ArrayAdapter<String> wards) {
        mPreventProvince = true;
        mPreventDistrict = true;
        mWard.setAdapter(wards);
        mDistrict.setAdapter(districts);
        mProvince.setAdapter(provinces);
    }
}
