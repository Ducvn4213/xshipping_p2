package product.dg.xshipping.send_gig.receiver_info;

import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;

import java.util.List;

import product.dg.xshipping.model.ReceiverInfo;
import product.dg.xshipping.service.database.model.DistrictResponse;
import product.dg.xshipping.service.database.model.WardResponse;

public class ReceiverInfoInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void loadOldData(String name, String phone, String address, ArrayAdapter<String> provinceAdapter, ArrayAdapter<String> districtAdapter, ArrayAdapter<String> wardAdapter, int province, int district, int ward);
        void loadGeoData(ArrayAdapter<String> provinces, ArrayAdapter<String> districts, ArrayAdapter<String> wards);
    }

    interface ProvidedPresenterOps {
        void onNext(String name, String phone, String address, String province, String district, String ward);
        void onSelectProvince(int id);
        void onSelectDistrict(int id);
        void loadOldInfo();
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void moveNext();
        ArrayAdapter<String> createAdapterWithData(List<String> data);
        void loadGeoData(ArrayAdapter<String> provinces, ArrayAdapter<String> districts, ArrayAdapter<String> wards);
        void notifyChanged(ArrayAdapter<String> districtAdapter, ArrayAdapter<String> wardAdapter);
        void handleOnDistrictCallback(DistrictResponse data);
        void handleOnWardCallback(WardResponse data);
        void loadOldData(String name, String phone, String address, ArrayAdapter<String> provinceAdapter, ArrayAdapter<String> districtAdapter, ArrayAdapter<String> wardAdapter, int province, int district, int ward);
    }

    interface ProvidedModelOps {
        void onNext(String name, String phone, String address, String province, String district, String ward);
        ReceiverInfo getReceiverInfo();

        void loadDistricts(int province_id);
        void loadWards(int district_id);

        void handleOnDistrictCallback(DistrictResponse data);
        void handleOnWardCallback(WardResponse data);

        void loadOldData();
    }
}
