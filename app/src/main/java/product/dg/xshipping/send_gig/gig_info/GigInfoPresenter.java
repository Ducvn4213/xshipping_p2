package product.dg.xshipping.send_gig.gig_info;

import java.lang.ref.WeakReference;

import product.dg.xshipping.R;
import product.dg.xshipping.send_gig.SendGigFragment;
import product.dg.xshipping.send_gig.sender_info.SenderInfoFragment;
import product.dg.xshipping.util.Utils;

public class GigInfoPresenter implements GigInfoInterfaces.ProvidedPresenterOps, GigInfoInterfaces.RequiredPresenterOps {
    private WeakReference<GigInfoInterfaces.RequiredViewOps> mView;
    private GigInfoInterfaces.ProvidedModelOps mModel;

    GigInfoPresenter(GigInfoInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(GigInfoInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void enablePaymentInfo() {
        SenderInfoFragment senderInfoFragment = (SenderInfoFragment) SendGigFragment.getInstance().getTab(2);
        if (senderInfoFragment != null) {
            senderInfoFragment.enablePaymentInformation();
        }
    }

    @Override
    public void disablePaymentInfo() {
        SendGigFragment sendGigFragment = SendGigFragment.getInstance();
        if (sendGigFragment == null) {
            return;
        }

        SenderInfoFragment senderInfoFragment = (SenderInfoFragment) sendGigFragment.getTab(2);
        if (senderInfoFragment != null) {
            senderInfoFragment.disablePaymentInformation();
        }
    }

    @Override
    public void onBack() {
        SendGigFragment.getInstance().gotoTab(0);
    }

    @Override
    public void onNext(String name, String weight, String payment_method, String size, String date, String cod, String note) {
        if (name.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_name_missing);
            return;
        }

        if (weight.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_weight_missing);
            return;
        }

        int iw = Integer.parseInt(weight);
        if (iw > 10000) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_over_weight);
            return;
        }

        if (iw > 2000) {
            size = "Trung bình";
        }

        if (payment_method.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_payment_method_missing);
            return;
        }

        if (size.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_size_missing);
            return;
        }

        if (date.trim().equalsIgnoreCase("ngày lấy hàng")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_date_missing);
            return;
        }

        String rdate = date.split(":")[1].trim();
        if (rdate.equalsIgnoreCase(Utils.todayInDayFirst())) {
            int ch = Integer.parseInt(Utils.currentHours());
            if (ch >= 16) {
                mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_out_of_time);
                return;
            }
        }

        if (cod.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_cod_missing);
            return;
        }

        cod = cod.replace(".", "");
        int intCOD = Integer.parseInt(cod);
        if (intCOD > 2000000) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_cod_limit);
            return;
        }

        mModel.onNext(name, weight, payment_method, size, date, cod, note);
    }

    @Override
    public void onNext(String name, String weight, String payment_method, String size, String date, String note) {
        if (name.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_name_missing);
            return;
        }

        if (weight.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_weight_missing);
            return;
        }

        int iw = Integer.parseInt(weight);
        if (iw > 10000) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_over_weight);
            return;
        }

        if (iw > 2000) {
            size = "Trung bình";
        }

        if (payment_method.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_payment_method_missing);
            return;
        }

        if (size.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_size_missing);
            return;
        }

        if (date.trim().equalsIgnoreCase("ngày lấy hàng")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_date_missing);
            return;
        }

        String rdate = date.split(":")[1].trim();
        if (rdate.equalsIgnoreCase(Utils.todayInDayFirst())) {
            int ch = Integer.parseInt(Utils.currentHours());
            if (ch >= 16) {
                mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_gig_message_out_of_time);
                return;
            }
        }

        mModel.onNext(name, weight, payment_method, size, date, "", note);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void moveNext() {
        SendGigFragment.getInstance().gotoTab(2);
    }
}
