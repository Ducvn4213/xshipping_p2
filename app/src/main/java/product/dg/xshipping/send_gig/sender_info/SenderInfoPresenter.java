package product.dg.xshipping.send_gig.sender_info;

import java.lang.ref.WeakReference;

import product.dg.xshipping.MainActivity;
import product.dg.xshipping.R;
import product.dg.xshipping.model.SenderInfo;
import product.dg.xshipping.send_gig.SendGigFragment;

public class SenderInfoPresenter implements SenderInfoInterfaces.ProvidedPresenterOps, SenderInfoInterfaces.RequiredPresenterOps {
    private WeakReference<SenderInfoInterfaces.RequiredViewOps> mView;
    private SenderInfoInterfaces.ProvidedModelOps mModel;

    SenderInfoPresenter(SenderInfoInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(SenderInfoInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void onBack() {
        SendGigFragment.getInstance().gotoTab(1);
    }

    @Override
    public void moveToFirst() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SendGigFragment.getInstance().gotoTab(0);
            }
        });
    }

    @Override
    public void presentDistanceAndCost(final String distance, final String code) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().presentDistanceAndCost(distance, code);
            }
        });
    }

    @Override
    public void onFinish(String name, String phone, String address) {
        if (name.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_name_missing);
            return;
        }

        if (phone.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_phone_missing);
            return;
        }

        if (address.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_address_missing);
            return;
        }

        mView.get().showWaiting();
        mModel.onFinish(name, phone, address, "", "", "");
    }

    @Override
    public void onFinish(String name, String phone, String address, String bank_name, String bank_number, String bank_bank) {
        if (name.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_name_missing);
            return;
        }

        if (phone.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_phone_missing);
            return;
        }

        if (address.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_address_missing);
            return;
        }

        if (bank_name.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_bank_name_missing);
            return;
        }

        if (bank_number.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_bank_number_missing);
            return;
        }

        if (bank_bank.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.send_gigs_sender_message_bank_bank_missing);
            return;
        }

        mView.get().showWaiting();
        mModel.onFinish(name, phone, address, bank_name, bank_number, bank_bank);
    }

    @Override
    public void loadOldData() {
        SenderInfo senderInfo = mModel.getSenderInfo();
        if (senderInfo != null) {
            mView.get().loadOldData(senderInfo.getName(), senderInfo.getPhone(), senderInfo.getAddress(), senderInfo.getBankName(), senderInfo.getBankNumber(), senderInfo.getBankBank());
        }
    }

    @Override
    public void saveData(String name, String phone, String address, String bank_name, String bank_number, String bank_branch) {
        mModel.saveData(name, phone, address, bank_name, bank_number, bank_branch);
    }

    @Override
    public void createGigTicket() {
        mView.get().showWaiting();
        mModel.createGigTicket();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void moveFinish() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MainActivity mainActivity = (MainActivity) mView.get().getParentActivity();
                mainActivity.goToTab(1);
            }
        });
    }
}
