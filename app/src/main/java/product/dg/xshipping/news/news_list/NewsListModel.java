package product.dg.xshipping.news.news_list;


import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.NewsCategoryList;
import product.dg.xshipping.adapter.model.NewsList;
import product.dg.xshipping.model.News;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class NewsListModel implements NewsListInterfaces.ProvidedModelOps {
    private NewsListInterfaces.RequiredPresenterOps mPresenter;
    private FSService mService = FSService.getInstance();
    private List<News> mData;
    NewsListModel(NewsListInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void loadData(String cate) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_cate", cate));

        mService.loadNewsList(paramList, new FSService.Callback<NewsList>() {
            @Override
            public void onSuccess(NewsList data) {
                mData = data.getData();
                mPresenter.presentData(data.getData());
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public News getData(int pos) {
        return mData.get(pos);
    }
}
