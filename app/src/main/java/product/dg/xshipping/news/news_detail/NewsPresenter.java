package product.dg.xshipping.news.news_detail;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipping.model.NewsCategory;

public class NewsPresenter implements NewsInterfaces.ProvidedPresenterOps, NewsInterfaces.RequiredPresenterOps{
    private WeakReference<NewsInterfaces.RequiredViewOps> mView;
    private NewsInterfaces.ProvidedModelOps mModel;

    NewsPresenter(NewsInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(NewsInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(NewsInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void loadData(String id) {
        mView.get().showWaiting();
        mModel.loadData(id);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void presentData(final String data) {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().presentData(data);
            }
        });
    }
}
