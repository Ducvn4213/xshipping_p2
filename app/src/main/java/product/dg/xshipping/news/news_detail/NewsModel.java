package product.dg.xshipping.news.news_detail;


import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.model.NewsCategoryList;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class NewsModel implements NewsInterfaces.ProvidedModelOps {
    private NewsInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    NewsModel(NewsInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void loadData(String id) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_news", id));

        mService.loadNews(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.presentData(data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }
}
