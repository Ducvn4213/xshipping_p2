package product.dg.xshipping.news.news_detail;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.adapter.CategoryNewsAdapter;
import product.dg.xshipping.manage_gig.wait.WaitGigFragment;
import product.dg.xshipping.model.NewsCategory;

public class NewsActivity extends AppCompatActivity implements NewsInterfaces.RequiredViewOps {

    private NewsInterfaces.ProvidedPresenterOps mPresenter;
    private ProgressDialog mProgressDialog;
    private String mID;
    private String mDate;
    private String mTitle;
    private String mSummary;
    TextView mTVTitle;
    TextView mTVDate;
    TextView mTVSummary;
    WebView mWebView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mTVTitle = (TextView) findViewById(R.id.tv_title);
        mTVDate = (TextView) findViewById(R.id.tv_date);
        mTVSummary = (TextView) findViewById(R.id.tv_summary);
        mWebView = (WebView) findViewById(R.id.wv_content);
    }

    void setupControlEvents() {
    }

    void init() {
        Bundle extras = getIntent().getExtras();
        mID = extras.getString("id");
        mTitle = extras.getString("title");
        mDate = extras.getString("date");
        mSummary = extras.getString("summary");

        NewsPresenter presenter = new NewsPresenter(this);
        NewsModel model = new NewsModel(presenter);

        presenter.setModel(model);
        mPresenter = presenter;

        mTVTitle.setText(mTitle);
        mTVDate.setText(mDate);
        mTVSummary.setText(mSummary);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.loadData(mID);
    }

    @Override
    public AppCompatActivity getActivity() {
        return NewsActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(NewsActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(NewsActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        NewsActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = ProgressDialog.show(NewsActivity.this, getString(R.string.dialog_message_waiting), "", true);
            }
        });
    }

    @Override
    public void hideWaiting() {
        NewsActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        });

    }

    @Override
    public void presentData(String data) {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadData(data, "text/html; charset=utf-8", "UTF-8");
        //CategoryNewsAdapter adapter = new CategoryNewsAdapter(getApplicationContext(), data);
        //mListView.setAdapter(adapter);
    }
}
