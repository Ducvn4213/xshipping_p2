package product.dg.xshipping.model;

public class GigInfo {
    String mName;
    String mWeight;
    String mPaymentMethod;
    String mSize;
    String mDate;
    String mCod;
    String mNote;

    public String getName() {
        return mName;
    }
    public void setName(String name) {
        this.mName = name;
    }

    public String getWeight() {
        return mWeight;
    }
    public void setWeight(String weight) {
        this.mWeight = weight;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }
    public void setPaymentMethod(String paymentMethod) {
        this.mPaymentMethod = paymentMethod;
    }

    public String getSize() {
        return mSize;
    }
    public void setSize(String size) {
        this.mSize = size;
    }

    public String getDate() {
        return mDate;
    }
    public void setDate(String date) {
        this.mDate = date;
    }

    public String getCod() {
        return mCod;
    }
    public void setCod(String cod) {
        this.mCod = cod;
    }

    public String getNote() {
        return mNote;
    }
    public void setNote(String note) {
        this.mNote = note;
    }

    public GigInfo() {}
}
