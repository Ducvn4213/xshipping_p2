package product.dg.xshipping.model;

import com.google.gson.annotations.SerializedName;

public class Shipper {
    @SerializedName("id")
    String mID;
    @SerializedName("account")
    String mAccount;
    @SerializedName("email")
    String mEmail;
    @SerializedName("name")
    String mName;
    @SerializedName("phone")
    String mPhone;
    @SerializedName("avatar")
    String mAvatar = "";
    @SerializedName("current_rating")
    String mCurRating = "3";
    @SerializedName("total_rating")
    String mTotalRating = "999";

    public String getAvatar() {
        return mAvatar;
    }

    public String getCurRating() {
        return mCurRating;
    }

    public String getTotalRating() {
        return mTotalRating;
    }

    public String getID() {
        return mID;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getAccountID() {
        return mAccount;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setPhone(String phone) {
        this.mPhone = phone;
    }
}
