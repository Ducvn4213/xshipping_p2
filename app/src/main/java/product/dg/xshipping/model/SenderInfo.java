package product.dg.xshipping.model;

public class SenderInfo {
    String mName;
    String mPhone;
    String mAddress;
    String mBankName;
    String mBankNumber;
    String mBankBank;

    public String getName() {
        return mName;
    }
    public void setName(String name) {
        this.mName = name;
    }

    public String getPhone() {
        return mPhone;
    }
    public void setPhone(String phone) {
        this.mPhone = phone;
    }

    public String getAddress() {
        return mAddress;
    }
    public void setAddress(String address) {
        this.mAddress = address;
    }

    public String getBankName() {
        if (mBankName == null) {
            return "";
        }
        return mBankName;
    }
    public void setBankName(String bankName) {
        this.mBankName = bankName;
    }

    public String getBankNumber() {
        if (mBankNumber == null) {
            return "";
        }
        return mBankNumber;
    }
    public void setBankNumber(String bankNumber) {
        this.mBankNumber = bankNumber;
    }

    public String getBankBank() {
        if (mBankBank == null) {
            return "";
        }
        return mBankBank;
    }
    public void setBankBank(String bankBank) {
        this.mBankBank = bankBank;
    }


    public SenderInfo() {}
}
