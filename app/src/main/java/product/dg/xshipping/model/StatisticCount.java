package product.dg.xshipping.model;

import com.google.gson.annotations.SerializedName;

public class StatisticCount {
    @SerializedName("id")
    String mID;
    @SerializedName("create_date")
    String mCreateDate;

    public String getID() {
        return mID;
    }

    public String getCreateDate() {
        return mCreateDate;
    }
}
