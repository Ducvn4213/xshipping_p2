package product.dg.xshipping.model;

public class Gig {
    GigInfo mGigInfo;
    ReceiverInfo mReceiverInfo;
    SenderInfo mSenderInfo;

    public GigInfo getGigInfo() {
        return mGigInfo;
    }
    public void setGigInfo(GigInfo mGigInfo) {
        this.mGigInfo = mGigInfo;
    }

    public ReceiverInfo getReceiverInfo() {
        return mReceiverInfo;
    }
    public void setReceiverInfo(ReceiverInfo mReceiverInfo) {
        this.mReceiverInfo = mReceiverInfo;
    }

    public SenderInfo getSenderInfo() {
        return mSenderInfo;
    }
    public void setSenderInfo(SenderInfo mSenderInfo) {
        this.mSenderInfo = mSenderInfo;
    }

    public Gig() {
        mGigInfo = new GigInfo();
        mReceiverInfo = new ReceiverInfo();
        mSenderInfo = new SenderInfo();
    }
}
