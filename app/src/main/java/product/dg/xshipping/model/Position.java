package product.dg.xshipping.model;

import com.google.gson.annotations.SerializedName;

public class Position {
    @SerializedName("latitude")
    double mLatitude;
    @SerializedName("longitude")
    double mlongitude;

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public void setLongitude(double longitude) {
        mlongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mlongitude;
    }

    public Position() {}
}
