package product.dg.xshipping;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipping.adapter.model.GigItem;

public class MainInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void loginSuccessCallback();
        void checkingWithData(List<GigItem> data);
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void doLogin(String username, String password);
        void doLoginFacebook(String id, String email, String name);
        void checkOldTicket();
        void pushThat(List<GigItem> data);
        void cancelThat(List<GigItem> data);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void onLoginSuccess();
        void checkingWithData(List<GigItem> data);
    }

    interface ProvidedModelOps {
        void doLogin(String username, String password);
        void doLoginFacebook(String id, String email, String name);
        void checkOldTicket();
        void pushThat(List<GigItem> data);
        void cancelThat(List<GigItem> data);
    }
}
