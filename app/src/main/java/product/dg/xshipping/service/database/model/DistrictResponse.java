package product.dg.xshipping.service.database.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DistrictResponse {

    @SerializedName("data")
    List<District> mData;

    public List<District> getData() {
        return mData;
    }

    public List<String> getStringListDistrict() {
        List<String> return_value = new ArrayList<>();
        return_value.add("Chọn Quận/Huyện");

        for (District district : mData) {
            return_value.add(district.getName());
        }

        return return_value;
    }

    public DistrictResponse() {
        mData = new ArrayList<>();
    }
}
