package product.dg.xshipping.service.database.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WardResponse {

    @SerializedName("data")
    List<Ward> mData;

    public List<Ward> getData() {
        return mData;
    }

    public List<String> getStringListWard() {
        List<String> return_value = new ArrayList<>();
        return_value.add("Chọn Phường/Xã");

        for (Ward ward : mData) {
            return_value.add(ward.getName());
        }

        return return_value;
    }

    public WardResponse() {
        mData = new ArrayList<>();
    }
}
