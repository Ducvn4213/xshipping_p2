package product.dg.xshipping.service.cloud_messaging;


import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class NotificationActionService extends IntentService {
    public static String ACTION_YES = "yes";
    public static String ACTION_NO = "no";

    public NotificationActionService() {
        super(NotificationActionService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String action = intent.getAction();
        String[] actionSplit = action.split("/");
        action = actionSplit[0];
        String code = actionSplit[1];
        String id = actionSplit[2];

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_ticket", id));

        if (ACTION_YES.equalsIgnoreCase(action)) {
            paramList.add(new Param("_result", "yes"));
        }
        else if (ACTION_NO.equalsIgnoreCase(action)) {
            paramList.add(new Param("_result", "no"));
        }

        FSService mService = FSService.getInstance();
        if (code.equalsIgnoreCase("vctxd")) {
            mService.confirmFromBookWaiting(paramList, new FSService.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                    //mPresenter.requestReload();
                }

                @Override
                public void onFail(String error) {
                    //mPresenter.requestReload();
                }
            });
        }
        else {
            mService.confirmFromWaitingPick(paramList, new FSService.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                    //mPresenter.requestReload();
                }

                @Override
                public void onFail(String error) {
                    //mPresenter.requestReload();
                }
            });
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancel(4213);
    }
}
