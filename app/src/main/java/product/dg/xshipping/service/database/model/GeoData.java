package product.dg.xshipping.service.database.model;

import java.util.ArrayList;
import java.util.List;

public class GeoData {

    List<Province> mProvinces;

    public List<Province> getListProvince() {
        return mProvinces;
    }

    public Province getProvinceByID(String id) {
        for (Province province : mProvinces) {
            if (province.getID().equalsIgnoreCase(id)) {
                return province;
            }
        }

        return null;
    }

    public List<String> getStringListProvince() {
        List<String> return_value = new ArrayList<>();
        return_value.add("Chọn Tỉnh/Thành Phố");

        for (Province province : mProvinces) {
            return_value.add(province.getName());
        }

        return return_value;
    }

    public GeoData() {
        mProvinces = new ArrayList<>();
        Province province = new Province("74", "Bình Dương", "Tỉnh");
        mProvinces.add(province);
        province = new Province("79", "Hồ Chí Minh", "Thành Phố");
        mProvinces.add(province);
    }
}
