package product.dg.xshipping.register;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.login.LoginActivity;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.TrackerGPS;
import product.dg.xshipping.util.FS;
import product.dg.xshipping.util.Utils;
import product.dg.xshipping.verify.VerifyActivity;

public class RegisterActivity extends AppCompatActivity implements RegisterInterfaces.RequiredViewOps{

    Button mRegister;
    EditText mAddress;
    EditText mEmail;
    EditText mName;
    EditText mPassword;
    EditText mRePassword;

    ImageButton mGetCurrentAddress;

    ProgressDialog mProgressDialog;

    TrackerGPS mTrackerGPS;

    private RegisterInterfaces.ProvidedPresenterOps mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();

        bindingControl();
        setupControlEvents();
    }

    private void init() {
        RegisterPresenter presenter = new RegisterPresenter(this);
        RegisterModel model = new RegisterModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void bindingControl() {
        mRegister = (Button) findViewById(R.id.btn_register);
        mAddress = (EditText) findViewById(R.id.edt_address);
        mEmail = (EditText) findViewById(R.id.edt_email);
        mName = (EditText) findViewById(R.id.edt_name);
        mPassword = (EditText) findViewById(R.id.edt_password);
        mRePassword = (EditText) findViewById(R.id.edt_repassword);
        mGetCurrentAddress = (ImageButton) findViewById(R.id.ibtn_address);
    }

    private void setupControlEvents() {
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.layout_policy_dialog, null);
                dialogBuilder.setView(dialogView);

                TextView name = (TextView) dialogView.findViewById(R.id.edt_name);
                Button create = (Button) dialogView.findViewById(R.id.btn_create);
                Button cancel = (Button) dialogView.findViewById(R.id.btn_cancel);

                final android.app.AlertDialog alertDialog = dialogBuilder.create();

                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://timshipper.net/qap"));
                        startActivity(browserIntent);
                    }
                });

                create.setOnClickListener(null);
                create.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        doRegister();
                    }
                });

                cancel.setOnClickListener(null);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                if (!alertDialog.isShowing()) {
                    alertDialog.show();
                }
            }
        });

        mGetCurrentAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog dialog = new AlertDialog.Builder(RegisterActivity.this)
                        .setTitle(getString(R.string.dialog_notice_title))
                        .setMessage(getString(R.string.dialog_message_get_current_location))
                        .setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                fillCurrentAddress();
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create();
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            }
        });
    }

    void doRegister() {
        String email = mEmail.getText().toString();
        String name = mName.getText().toString();
        String address = mAddress.getText().toString();
        String password = mPassword.getText().toString();
        String repassword = mRePassword.getText().toString();

        if (!password.equals(repassword)) {
            RegisterActivity.this.showDialog(R.string.dialog_title_error, R.string.dialog_message_password_doesnt_match);
            return;
        }

        mPresenter.doRegister(email, name, address, password);
    }

    @Override
    public AppCompatActivity getActivity() {
        return RegisterActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(RegisterActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(RegisterActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void registerSuccessCallback() {
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        FSService.getInstance().saveUserCredential(RegisterActivity.this, email, password);
        mPresenter.moveToVerify();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    private void fillCurrentAddress() {
        checkLocationPermission();
    }

    private void doFillCurrentAddress() {
        mTrackerGPS = new TrackerGPS(RegisterActivity.this);
        if(mTrackerGPS.canGetLocation()){

            double longitude = mTrackerGPS.getLongitude();
            double latitude = mTrackerGPS.getLatitude();

            if (longitude == 0 && latitude == 0) {
                doFillCurrentAddress();
                return;
            }

            Geocoder geocoder = new Geocoder(RegisterActivity.this);
            try {
                Address address = geocoder.getFromLocation(latitude, longitude, 1).get(0);
                String addressString = Utils.getStringAddressFromAddress(address);
                mAddress.setText(addressString);
            } catch (IOException e) {
                showDialog(R.string.dialog_title_error, R.string.dialog_message_get_current_location_fail);
            }
        }
        else
        {
            mTrackerGPS.showSettingsAlert();
        }

    }

    private void checkLocationPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        else {
            doFillCurrentAddress();
            return;
        }

        if (ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        else {
            doFillCurrentAddress();
            return;
        }

        String[] perArr = new String[permissions.size()];
        perArr = permissions.toArray(perArr);
        ActivityCompat.requestPermissions(RegisterActivity.this,
                perArr,
                FS.PERMISSIONS_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == FS.PERMISSIONS_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    doFillCurrentAddress();
                }
                else {
                    showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_location_permission);
                }
            }
            else {
                showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_location_permission);
            }
        }
    }
}
