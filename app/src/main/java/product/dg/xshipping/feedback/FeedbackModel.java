package product.dg.xshipping.feedback;


import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

public class FeedbackModel implements FeedbackInterfaces.ProvidedModelOps {
    private FeedbackInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    FeedbackModel(FeedbackInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void send(String data) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_content", data));

        mService.sendFeedback(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.sendSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }
}
