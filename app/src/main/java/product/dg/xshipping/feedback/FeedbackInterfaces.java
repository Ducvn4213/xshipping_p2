package product.dg.xshipping.feedback;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipping.model.NewsCategory;

public class FeedbackInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void sendSuccess();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void send(String content);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void sendSuccess();
    }

    interface ProvidedModelOps {
        void send(String content);
    }
}
