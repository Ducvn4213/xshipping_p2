package product.dg.xshipping.promotion;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.CallbackManager;

import product.dg.xshipping.R;


public class PromotionActivity extends AppCompatActivity implements PromotionInterfaces.RequiredViewOps  {
    ProgressDialog mProgressDialog;

    EditText mCode;
    Button mSubmit;

    private PromotionInterfaces.ProvidedPresenterOps mPresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_promotion_code);

        init();

        bindingControl();
        setupControlEvents();
    }

    private void init() {
        PromotionPresenter presenter = new PromotionPresenter(PromotionActivity.this);
        PromotionModel model = new PromotionModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    void bindingControl() {
        mCode = (EditText) findViewById(R.id.edt_code);
        mSubmit = (Button) findViewById(R.id.btn_submit);
    }

    void setupControlEvents() {
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.submitCode(mCode.getText().toString());
            }
        });
    }

    @Override
    public AppCompatActivity getActivity() {
        return PromotionActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(PromotionActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(PromotionActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(PromotionActivity.this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void promotionResultCallback() {
        final AlertDialog dialog = new AlertDialog.Builder(PromotionActivity.this)
                .setTitle(getString(R.string.dialog_notice_title))
                .setMessage(getString(R.string.promotion_code_success))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        PromotionActivity.this.finish();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
