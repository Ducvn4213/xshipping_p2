package product.dg.xshipping.promotion;

import android.support.v7.app.AppCompatActivity;

public class PromotionInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void promotionResultCallback();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void submitCode(String code);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void submitPromotionCodeSuccess();
    }

    interface ProvidedModelOps {
        void submitCode(String code);
    }
}
