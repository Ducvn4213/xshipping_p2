package product.dg.xshipping.promotion;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;

/**
 * Created by dg on 3/30/17.
 */

public class PromotionModel implements PromotionInterfaces.ProvidedModelOps {
    private PromotionInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    PromotionModel(PromotionInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void submitCode(String code) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_code", code));

        mService.submitPromotionCode(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                if (mService.getCurrentFacebookUser() != null) {
                    User faceUser = mService.getCurrentFacebookUser();
                    data.setEmail(faceUser.getEmail());
                    mService.setCurrentFacebookUser(data);
                }
                else {
                    mService.setCurrentUser(data);
                }
                mPresenter.submitPromotionCodeSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }
}
