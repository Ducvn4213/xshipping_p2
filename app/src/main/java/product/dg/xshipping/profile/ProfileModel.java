package product.dg.xshipping.profile;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipping.R;
import product.dg.xshipping.model.User;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.service.network.Param;
import product.dg.xshipping.util.Utils;

public class ProfileModel implements ProfileInterfaces.ProvidedModelOps{
    private User mCurrentUser;
    private User mCurrentFacebookUser;

    private Bitmap mCurrentAvatarBitmap;

    private ProfileInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    ProfileModel(ProfileInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setCurrentAvatarBitmap(Bitmap bitmap) {
        this.mCurrentAvatarBitmap = bitmap;
    }

    @Override
    public void doUpdateAvatar() {
        if (mCurrentAvatarBitmap == null) {
            mPresenter.showDialog(R.string.dialog_title_error, R.string.dialog_message_avatar_invalid);
            return;
        }

        Utils.ConvertBitmapToStringTask convertBitmapToStringTask = new Utils.ConvertBitmapToStringTask();
        convertBitmapToStringTask.setBitmap(mCurrentAvatarBitmap);
        convertBitmapToStringTask.setCallback(new Utils.ConvertBitmapToStringCallback() {
            @Override
            public void onSuccess(String bitmapString) {
                doUpdateAvatar(bitmapString);
            }

            @Override
            public void onFail() {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.dialog_message_avatar_invalid);
            }
        });
        convertBitmapToStringTask.execute();
    }

    private void doUpdateAvatar(String bitmapString) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_image", bitmapString));

        mService.update_avatar(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.showDialog(R.string.dialog_notice_title, R.string.dialog_message_avatar_update_success);
                mPresenter.updateAvatarSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.dialog_message_avatar_invalid);
            }
        });
    }

    @Override
    public void doUpdate(String name, String address, String phone, String bank_name, String bank_number, String bank_branch) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_name", name));
        paramList.add(new Param("_phone", phone));
        paramList.add(new Param("_address", address));
        paramList.add(new Param("_bank_name", bank_name));
        paramList.add(new Param("_bank_number", bank_number));
        paramList.add(new Param("_bank_branch", bank_branch));


        mService.update_profile(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                if (mService.getCurrentFacebookUser() != null) {
                    User faceUser = mService.getCurrentFacebookUser();
                    data.setEmail(faceUser.getEmail());
                    mService.setCurrentFacebookUser(data);
                }
                else {
                    mService.setCurrentUser(data);
                }
                mPresenter.onUpdateSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void setFacebookUser(User user) {
        this.mCurrentFacebookUser = null;
        this.mCurrentUser = null;

        this.mCurrentFacebookUser = user;
    }

    @Override
    public void setUser(User user) {
        this.mCurrentFacebookUser = null;
        this.mCurrentUser = null;

        this.mCurrentUser = user;
    }

    @Override
    public User getFacebookUser() {
        return this.mCurrentFacebookUser;
    }

    @Override
    public User getuser() {
        return this.mCurrentUser;
    }

    @Override
    public Boolean isFacebookUser() {
        return this.mCurrentFacebookUser != null;
    }

    @Override
    public void displayUserInfo() {
        if (mService.getCurrentUser() == null) {
            setFacebookUser(mService.getCurrentFacebookUser());
        }
        else {
            setUser(mService.getCurrentUser());
        }

        User currentUser = isFacebookUser() ? mCurrentFacebookUser : mCurrentUser;
        if (currentUser == null) {
            mPresenter.displayUserInfo_None();
        }
        else {
            String avatar = currentUser.getAvatar();
            if (mCurrentAvatarBitmap != null) {
                avatar = null;
            }
            mPresenter.displayUserInfo(currentUser.getName(), currentUser.getEmail(), currentUser.getAddress(), currentUser.getPhone(), avatar, currentUser.getBankName(), currentUser.getBankNumber(), currentUser.getBankBranch(), currentUser.isActive(), isFacebookUser());
        }
    }
}
