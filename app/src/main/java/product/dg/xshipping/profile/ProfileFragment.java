package product.dg.xshipping.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;

import product.dg.xshipping.R;
import product.dg.xshipping.service.FSService;
import product.dg.xshipping.util.FS;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment implements ProfileInterfaces.RequiredViewOps{

    ProgressDialog mProgressDialog;
    private ProfileInterfaces.ProvidedPresenterOps mPresenter;

    EditText mName;
    EditText mEmail;
    EditText mPhone;
    EditText mAddress;
    EditText mBankName;
    EditText mBankNumber;
    EditText mBankBranch;

    Button mVerifyPhoneNumber;
    Button mChangePassword;
    Button mLogin;
    TextView mNameView;
    TextView mEmailView;
    ImageButton mChangeImage;
    ImageButton mApplyImage;
    ImageView mAvatar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_profile, container, false);

        init();

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    private void init() {
        ProfilePresenter presenter = new ProfilePresenter(this);
        ProfileModel model = new ProfileModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    private void bindingControls(View view) {
        mName = (EditText) view.findViewById(R.id.edt_name);
        mEmail = (EditText) view.findViewById(R.id.edt_email);
        mPhone = (EditText) view.findViewById(R.id.edt_phone);
        mAddress = (EditText) view.findViewById(R.id.edt_address);
        mBankName = (EditText) view.findViewById(R.id.edt_bank_account_name);
        mBankNumber = (EditText) view.findViewById(R.id.edt_bank_account_number);
        mBankBranch = (EditText) view.findViewById(R.id.edt_bank_account_bank);

        mVerifyPhoneNumber = (Button) view.findViewById(R.id.btn_verify);
        mChangePassword = (Button) view.findViewById(R.id.btn_change_password);
        mLogin = (Button) view.findViewById(R.id.btn_login);
        mNameView = (TextView) view.findViewById(R.id.tv_name);
        mEmailView = (TextView) view.findViewById(R.id.tv_email);
        mChangeImage = (ImageButton) view.findViewById(R.id.ibtn_change_image);
        mAvatar = (ImageView) view.findViewById(R.id.iv_avatar);
        mApplyImage = (ImageButton) view.findViewById(R.id.ibtn_apply_image);
    }

    private void setupControlEvents() {
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.openLogin();
            }
        });

        mVerifyPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.openVerify();
            }
        });

        mChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.openChangePassword();
            }
        });

        mChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.openImagePicker();
            }
        });

        mApplyImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.doUpdateAvatar();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.updateDisplayUserInfo();
    }

    @Override
    public Activity getParenActivity() {
        return ProfileFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParenActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParenActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(getParenActivity(), getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void updateSuccessCallback() {
        mPresenter.updateDisplayUserInfo();
    }

    @Override
    public void displayUserInfo(String name, String email, String address, String phone, String avatar, String bank_name, String bank_number, String bank_branch, boolean isActive, boolean isFacebookLogged) {
        mName.setVisibility(View.VISIBLE);
        mEmail.setVisibility(View.VISIBLE);
        mPhone.setVisibility(View.VISIBLE);
        mAddress.setVisibility(View.VISIBLE);
        mBankName.setVisibility(View.VISIBLE);
        mBankNumber.setVisibility(View.VISIBLE);
        mBankBranch.setVisibility(View.VISIBLE);


        mLogin.setVisibility(View.GONE);
        mChangeImage.setVisibility(View.VISIBLE);

        mName.setText(name);
        mNameView.setText(name);
        mEmail.setText(email);
        mEmailView.setText(email);
        mPhone.setText(phone);
        mAddress.setText(address);
        mBankName.setText(bank_name);
        mBankNumber.setText(bank_number);
        mBankBranch.setText(bank_branch);

        if (avatar != null && !avatar.trim().equalsIgnoreCase("")) {
            Picasso.with(getParenActivity()).invalidate(FS.IMAGE_HOST + avatar);
            Picasso.with(getParenActivity()).load(FS.IMAGE_HOST + avatar).into(mAvatar);
        }

        if (isActive) {
            mVerifyPhoneNumber.setVisibility(View.GONE);
        }
        else {
            mVerifyPhoneNumber.setVisibility(View.VISIBLE);
        }

        if (isFacebookLogged) {
            mChangePassword.setVisibility(View.GONE);
        }
        else {
            mChangePassword.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void displayUserInfo_None() {
        displayUserInfo("", "", "", "", "", "", "", "", true, false);

        mName.setVisibility(View.GONE);
        mEmail.setVisibility(View.GONE);
        mPhone.setVisibility(View.GONE);
        mAddress.setVisibility(View.GONE);
        mBankName.setVisibility(View.GONE);
        mBankNumber.setVisibility(View.GONE);
        mBankBranch.setVisibility(View.GONE);

        mVerifyPhoneNumber.setVisibility(View.GONE);
        mChangePassword.setVisibility(View.GONE);

        mLogin.setVisibility(View.VISIBLE);
        mChangeImage.setVisibility(View.GONE);

        mAvatar.setImageResource(R.drawable.profile_default);

        mNameView.setText(getString(R.string.profile_name_not_logged));
        mEmailView.setText(getString(R.string.profile_email_not_logged));
    }

    @Override
    public void updateAvatarSuccess() {
        mChangeImage.setVisibility(View.VISIBLE);
        mApplyImage.setVisibility(View.GONE);
        mPresenter.setCurrentAvatarBitmap(null);
    }

    public Boolean isEdit() {
        return mPhone.isEnabled();
    }

    public void enableEdit() {
        FSService service = FSService.getInstance();

        mPhone.setEnabled(true);
        mAddress.setEnabled(true);
        mBankName.setEnabled(true);
        mBankNumber.setEnabled(true);
        mBankBranch.setEnabled(true);


        if (service.getCurrentFacebookUser() == null) {
            mName.setEnabled(true);
            mName.requestFocus();
        }
        else {
            mName.setEnabled(false);
            mAddress.requestFocus();
        }

        mChangePassword.setVisibility(View.GONE);
        mVerifyPhoneNumber.setVisibility(View.GONE);
    }

    public void disableEdit() {
        mName.setEnabled(false);
        mPhone.setEnabled(false);
        mAddress.setEnabled(false);
        mBankName.setEnabled(false);
        mBankNumber.setEnabled(false);
        mBankBranch.setEnabled(false);

        mName.requestFocus();
        mChangePassword.setVisibility(View.VISIBLE);
    }

    public void doUpdateProfileInfo() {
        disableEdit();
        String name = mName.getText().toString();
        String phone = mPhone.getText().toString();
        String address = mAddress.getText().toString();
        String bank_name = mBankName.getText().toString();
        String bank_number = mBankNumber.getText().toString();
        String bank_branch = mBankBranch.getText().toString();


        mPresenter.doUpdate(name, address, phone, bank_name, bank_number, bank_branch);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FS.PICK_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
            changeImage(filePath);
        }
    }

    private void changeImage(Uri filePath) {
        try {
            mChangeImage.setVisibility(View.GONE);
            mApplyImage.setVisibility(View.VISIBLE);
            Picasso.with(getParenActivity()).load(filePath).into(mAvatar);

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
            mPresenter.setCurrentAvatarBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
